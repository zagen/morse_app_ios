//
//  TextCodecTest.m
//  MorseApp
//
//  Created by Admin on 11.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TextCodec.h"

@interface TextCodecTest : XCTestCase

@end

@implementation TextCodecTest

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testPositiveSetMorseCodeWithMistakes
{
    TextCodec * codec = [[TextCodec alloc]init];
    [codec setMorse:@"-. .-ew"];
    XCTAssertEqualObjects(@"-. .-", codec.encodedText, @"Setting text not processed into morse code");
    
}

- (void) testPositiveSetMorseCode
{
    TextCodec *codec = [[TextCodec alloc]init];
    [codec setMorse:@"--."];
    XCTAssertEqualObjects(@"--.", codec.encodedText, @"setting right morse code ok");
}
- (void) testPositiveEncodingTextToMorse
{
    TextCodec *codec = [[TextCodec alloc]init];
    [codec setLocale:RUS];
    [codec setText:@"СОС"];
    XCTAssertEqualObjects(codec.encodedText, @"... --- ...", @"Simple sos check");
    [codec setLocale:ALL];
    [codec setText:@"SOSСОС"];
    XCTAssertEqualObjects(codec.encodedText, @"... --- ... ... --- ...", @"mixed rus/int(all) sos check");
}
-(void) testPositiveDecodingMorseCode
{
    TextCodec *codec = [[TextCodec alloc]init];
    [codec setLocale:INT];
    [codec setMorse:@"... --- ..."];
    XCTAssertEqualObjects(codec.decodedText, @"SOS", @"Simple sos decoding check");
}

@end
