//
//  WordAccessorText.m
//  MorseApp
//
//  Created by Admin on 15.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WordAccessor.h"

@interface WordAccessorTest : XCTestCase

@end

@implementation WordAccessorTest

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testGetFirstWordPositive
{
    WordAccessor *wa = [WordAccessor sharedWordAccessor];
    wa.locale = RUS;
    
    XCTAssertEqualObjects(@"быть", [wa wordByIndex:0], @"Test reading text from file with words");
    wa.locale = INT;
    XCTAssertEqualObjects(@"all", [wa wordByIndex:4], @"Test reading text from file with words");
    [wa randomWord];
}

@end
