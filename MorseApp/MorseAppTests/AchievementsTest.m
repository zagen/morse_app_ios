//
//  AchievementsTest.m
//  MorseApp
//
//  Created by Admin on 25.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Achievement.h"
#import "NewbieAchievement.h"

@interface AchievementsTest : XCTestCase

@end

@implementation AchievementsTest

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testCountOfCorrectCharacters
{
    Achievement *achievement = [[NewbieAchievement alloc]init];
    NSArray *results = @[
                         @{@"S":@"SS"}
                        ];
    [achievement countCharacters:results];
    XCTAssertTrue(achievement.countOfCorrectInputedChars == 1, @"S != SS");
    
    results = @[
                         @{@"WSO":@"WW"}
                         ];
    achievement = [[NewbieAchievement alloc]init];
    [achievement countCharacters:results];
    XCTAssertTrue(achievement.countOfCorrectInputedChars == 1, @"WSO - WW");

    results = @[
                @{@"AGS":@"AGS"}
                ];
    achievement = [[NewbieAchievement alloc]init];
    [achievement countCharacters:results];
    XCTAssertTrue(achievement.countOfCorrectInputedChars == 3, @"AGS - AGS");
}

- (void)testCountOfTotalCharacters
{
    Achievement *achievement = [[Achievement alloc]init];
    NSArray *results = @[
                         @{@"S":@"SS"}
                         ];
    [achievement countCharacters:results];
    XCTAssertTrue(achievement.countOfTotalInputedChars == 1, @"S != SS");
    
    results = @[
                @{@"WSO":@"WW"}
                ];
    achievement = [[Achievement alloc]init];
    [achievement countCharacters:results];
    XCTAssertTrue(achievement.countOfTotalInputedChars == 3, @"WSO - WW");
    
    results = @[
                @{@"AGS":@"AGS"}
                ];
    achievement = [[Achievement alloc]init];
    [achievement countCharacters:results];
    XCTAssertTrue(achievement.countOfTotalInputedChars == 3, @"AGS - AGS");
}
- (void)testNewbieNamePositive
{
    Achievement *achievement = [[NewbieAchievement alloc]init];
    XCTAssertEqualObjects(achievement.name, @"Newbie", @"Newbie achievement names not equal");
}


-(void)testNewbieToFitConditionsPositive{
    Achievement *achievement = [[NewbieAchievement alloc]init];
    NSArray *results = @[
                         @{@"A":@"A"},
                         @{@"S":@"S"},
                         @{@"W":@"W"},
                         @{@"G":@"G"},
                         @{@":":@":"}
                         
                         ];
    
    XCTAssertTrue([achievement fitToConditios:results], @"Newbie achieved test ToFitConditionsPositive");
}

-(void)testNewbieToFitConditionsLessThen5RCorrectNegative{
    Achievement *achievement = [[NewbieAchievement alloc]init];
    NSArray *results = @[
                         @{@"BB":@"BB"},
                         @{@"S":@"S"},
                         @{@"A":@"W"},
                         @{@"GV":@"G"},
                         @{@"C":@":"}
                         
                         ];
    
    XCTAssertFalse([achievement fitToConditios:results], @"Newbie achieved test ToFitConditionsLessThen5RCorrectNegative");
}

@end
