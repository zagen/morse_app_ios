//
//  AchievementManagerTests.m
//  MorseApp
//
//  Created by Admin on 26.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AchievementsManager.h"
#import "Settings.h"
#import "Achievement.h"

@interface AchievementManagerTests : XCTestCase

@end

@implementation AchievementManagerTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testGetAllSubclassesOfAchievementClassPositive
{
    XCTAssertEqual([[AchievementsManager allAchievementSubclasses] count], 6, @"Achievements count isnt equal to 6");
}

-(void)testAnalyzePersonalRecordOfOneCorrectPositive
{
    AchievementsManager * manager = [AchievementsManager sharedAchievementsManager];
    Settings *settings = [Settings sharedSettings];
    manager.settings = settings;
    [settings resetToDefaults];
    NSArray *resultData = @[
                            @{@"1": @"1"}
                            ];
    NSArray * achievements = [manager analyze:resultData];
    XCTAssertTrue([achievements count] == 1, @"Only personal best achieved. New achievement - 1");
    XCTAssertTrue([[achievements[0] name] isEqualToString:@"Personal record"], @"Only personal best achieved");
    resultData = @[
                   @{@"1": @"1"},
                   @{@"d": @"d"},
                   @{@"a": @"a"}
                   ];
    [manager analyze:resultData];
    XCTAssertTrue([manager.achieved count] == 1, @"Only personal best achieved. Total achievements - %d", [manager.achieved count]);
    Achievement * achievement = manager.achieved[0];
    XCTAssertTrue([achievement.name isEqualToString:@"Personal record"] && achievement.countOfCorrectInputedChars == 3, @"Only personal best achieved. Correct : %d", achievement.countOfCorrectInputedChars);
    resultData = @[
                   @{@"1": @"1"},
                   @{@"d": @"d"},
                   @{@"a": @"a"},
                   @{@"d": @"d"},
                   @{@"a": @"a"},
                   @{@"d": @"d"}
                   ];
    [manager analyze:resultData];
    XCTAssertTrue([manager.achieved count] == 2, @"Newbie and personal best. Total achievements - %d", [manager.achieved count]);

}
@end
