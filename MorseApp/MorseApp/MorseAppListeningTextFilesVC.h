//
//  MorseAppListeningTextFilesVC.h
//  MorseApp
//
//  Created by Admin on 03.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MorseAppSelectTextFileDelegate.h"
#import "General/TransmitterDelegate.h"

@interface MorseAppListeningTextFilesVC : UIViewController<MorseAppSelectTextFileDelegate, UIAlertViewDelegate, TransmitterDelegate>

@end
