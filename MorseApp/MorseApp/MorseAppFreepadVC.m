//
//  MorseAppFreepadVC.m
//  MorseApp
//
//  Created by Admin on 12.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "MorseAppFreepadVC.h"
#import "TouchPadView.h"
#import "Settings.h"
#import "General/TextCodec.h"

@interface MorseAppFreepadVC ()
@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *currentLocaleSegControl;
@property (weak, nonatomic) IBOutlet TouchPadView *touchPad;
@property (weak, nonatomic) IBOutlet UIButton *backspaceButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteMessageButton;
@property (weak, nonatomic) IBOutlet UIButton *shareMessageButton;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (strong, nonatomic) Settings * settings;
@property (strong, nonatomic) TextCodec * codec;
@property (strong, nonatomic) NSMutableString *message;
@property (strong, nonatomic) NSMutableString *currentMorseCode;
@end

@implementation MorseAppFreepadVC

-(NSMutableString *)message
{
    if (!_message) {
        _message = [[NSMutableString alloc]init];
    }
    return _message;
}
-(Settings *)settings
{
    if (!_settings) {
        _settings = [Settings sharedSettings];
    }
    return _settings;
}
-(TextCodec *)codec
{
    if (!_codec) {
        _codec = [[TextCodec alloc]init];
    }
    return _codec;
}

-(NSMutableString *)currentMorseCode
{
    if (!_currentMorseCode) {
        _currentMorseCode = [[NSMutableString alloc]init];
    }
    return _currentMorseCode;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.touchPad.delegate = self;
    self.okButton.layer.cornerRadius = self.okButton.bounds.size.width/2;
    self.backspaceButton.layer.cornerRadius = self.backspaceButton.bounds.size.width/2;
    self.messageTextView.layer.cornerRadius = 5;
    [self.currentLocaleSegControl setSelectedSegmentIndex:self.settings.locale];
    self.message = [[NSMutableString alloc]initWithString:self.messageTextView.text];
    
}


- (IBAction)ok:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)backspace:(id)sender {
    if ([self.message length]) {
        [self.message deleteCharactersInRange:NSMakeRange([self.message length] - 1, 1)];
        self.messageTextView.text = self.message;
    }
    
}
- (IBAction)deleteMessage:(id)sender {
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Deleting inputed text" message:@"Are you sure you want to delete inputed text?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [alert show];
    alert = nil;
}
- (IBAction)changeLocale:(UISegmentedControl *)sender {
    self.codec.locale = sender.selectedSegmentIndex;
}
- (IBAction)share:(id)sender {
    if ([self.message length]) {
        UIActivityViewController *controller =
        [[UIActivityViewController alloc]
         initWithActivityItems:@[self.message]
         applicationActivities:nil];
        controller.excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                             UIActivityTypeAssignToContact,
                                             UIActivityTypeSaveToCameraRoll,
                                             UIActivityTypeAddToReadingList,
                                             UIActivityTypePostToFlickr,
                                             UIActivityTypePostToVimeo,
                                             UIActivityTypePostToTencentWeibo,
                                             UIActivityTypeAirDrop];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        self.message = [[NSMutableString alloc]init];
        self.messageTextView.text = self.message;
    }
}
-(void)inputed:(NSString *)symbol
{
    if ([symbol isEqualToString:@" "]) {
        [self.codec setMorse:self.currentMorseCode];
        [self.message appendString:self.codec.decodedText];;
        self.messageTextView.text = self.message;
        [self.currentMorseCode setString:@""];
    }else if([symbol isEqualToString:@"   "]){
        [self.currentMorseCode setString:@""];
        [self.message appendString:@" "];
        self.messageTextView.text = self.message;
    }else{
        [self.currentMorseCode appendString:symbol];
    }
}
@end
