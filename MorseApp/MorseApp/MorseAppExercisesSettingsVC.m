//
//  MorseAppExercisesSettingsVC.m
//  MorseApp
//
//  Created by Admin on 14.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "MorseAppExercisesSettingsVC.h"
#import "SliderCell.h"
#import "Settings.h"
#import "General/TextCodec.h"
#import "NSString+NSString_containsCategory.h"

@interface MorseAppExercisesSettingsVC ()
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) Settings * settings;
@property (strong, nonatomic) NSMutableString * abc;
@property (strong, nonatomic) NSString * fullAbc;

@end

@implementation MorseAppExercisesSettingsVC

-(NSMutableString *)abc
{
    if (!_abc ) {
        if (self.settings.locale ==RUS) {
            _abc = [self.settings.currentABCRus mutableCopy];
        }else{
            _abc = [self.settings.currentABCInt mutableCopy];
        }
    }
    return _abc;
}
-(NSString *)fullAbc
{
    if (!_fullAbc) {
        if (self.settings.locale ==RUS) {
            _fullAbc = SETTINGS_ABC_CYRILLYC;
        }else{
            _fullAbc = SETTINGS_ABC_LATIN;
        }
    }
    return _fullAbc;
}

-(Settings *)settings
{
    if (!_settings) {
        _settings = [Settings sharedSettings];
    }
    return _settings;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.backButton.layer.cornerRadius = self.backButton.bounds.size.height/2;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
}



- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)tableTapped:(UIGestureRecognizer *)sender {
    CGPoint tapLocation = [sender locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:tapLocation];
    
    if (indexPath) { //we are in a tableview cell, let the gesture be handled by the view
        sender.cancelsTouchesInView = NO;
        if(indexPath.section == 0 && indexPath.row != self.settings.sequenceType)
        {
            NSIndexPath * indexPathOldLocale = [NSIndexPath indexPathForRow:self.settings.sequenceType
                                                                  inSection:0];
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPathOldLocale];
            cell.accessoryType = UITableViewCellAccessoryNone;
            self.settings.sequenceType = (int)indexPath.row;
            cell = [self.tableView cellForRowAtIndexPath:indexPath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [self.settings save];
        }else if ((indexPath.section == 1 && !self.isReceiveExercise) || indexPath.section == 3)
        {
            UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:indexPath];
            unichar currentChar = [self.fullAbc characterAtIndex:indexPath.row];
            NSString * currentCharString = [NSString stringWithCharacters:&currentChar length:1];
            
            NSLog(@"Abc before changing: %@", self.abc);
            if ([self.abc containsString:currentCharString] ) {
                if ([self.abc length] != 1) {
                    cell.accessoryType = UITableViewCellAccessoryNone;
                    NSRange entry = [self.abc rangeOfString:currentCharString];
                    [self.abc deleteCharactersInRange:entry];
                }
            }else{
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                [self.abc appendString:currentCharString];
            }
            NSLog(@"Abc after changing: %@", self.abc);
            
            if (self.settings.locale == RUS) {
                self.settings.currentABCRus = self.abc;
            }else{
                self.settings.currentABCInt = self.abc;
            }
            [self.settings save];
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifierCheck = @"SettingsCheck";
    static NSString *CellIdentifierSlider   = @"SettingsSlider";

    
    UITableViewCell * cell;
    SliderCell * cellWithSlider;
    switch (indexPath.section) {
        case 0:
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierCheck];
            cell.accessoryType = UITableViewCellAccessoryNone;
            if (indexPath.row == 0) {
                cell.textLabel.text = @"Characters";
            }else{
                cell.textLabel.text = @"Words";
            }
            if (indexPath.row == self.settings.sequenceType) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            break;
        case 1 :
            if (self.isReceiveExercise) {

                cellWithSlider = [tableView dequeueReusableCellWithIdentifier:CellIdentifierSlider];
                cellWithSlider.path = indexPath;
                cellWithSlider.delegate = self;
                cell = cellWithSlider;
                [cellWithSlider initSliderValues:NSMakeRange(1, 29)
                                         beginAt:self.settings.groupsNumber
                                                    step:1];

            }else{
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierCheck];
                cell.accessoryType = UITableViewCellAccessoryNone;
                unichar character = [self.fullAbc characterAtIndex:indexPath.row];
                cell.textLabel.text = [[NSString stringWithCharacters:&character length:1] uppercaseString];

                if ([self.abc containsString:[NSString stringWithCharacters:&character length:1]]) {
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
            }
            break;
        case 2:
            cellWithSlider = [tableView dequeueReusableCellWithIdentifier:CellIdentifierSlider];
            cellWithSlider.path = indexPath;
            cellWithSlider.delegate = self;
            cell = cellWithSlider;
            [cellWithSlider initSliderValues:NSMakeRange(0, 5)
                                     beginAt:self.settings.delayBeforeTransmission
                                        step:1];
            break;
        case 3:
        default:
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierCheck];
            cell.accessoryType = UITableViewCellAccessoryNone;
            unichar character = [self.fullAbc characterAtIndex:indexPath.row];
            cell.textLabel.text = [[NSString stringWithCharacters:&character length:1] uppercaseString];
            if ([self.abc containsString:[NSString stringWithCharacters:&character length:1]]) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }

            break;
    }
    
     return cell;

}
-(void)slidingDidStop:(NSIndexPath *)path atValue:(float)value
{
    if (path.section == 1) {
        self.settings.groupsNumber = (int)value;
        [self.settings save];
    }else if(path.section == 2)
    {
        self.settings.delayBeforeTransmission = (int)value;
        [self.settings save];
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *abc;
    switch (section) {
        case 0:
            return 2;
            break;
        case 1 :
            if (self.isReceiveExercise) {
                return 1;
            }else{
                 abc = (self.settings.locale == RUS)? SETTINGS_ABC_CYRILLYC: SETTINGS_ABC_LATIN;
                int length = [abc length];
                return length;
            }
            break;
        case 2:
            return 1;
            break;
        case 3:
        default:
            abc = (self.settings.locale == RUS)? SETTINGS_ABC_CYRILLYC: SETTINGS_ABC_LATIN;
            int length = [abc length];
            return length;
            break;
    }
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UILabel *headerView=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    headerView.backgroundColor=[UIColor clearColor];
    headerView.shadowColor = [UIColor darkGrayColor];
    headerView.shadowOffset = CGSizeMake(0,2);
    headerView.textColor = [UIColor whiteColor]; //here you can change the text color of header.
    
    headerView.font = [UIFont systemFontOfSize:14];
    
    
    NSString *sectionName;
    switch (section) {
        case 0:
            sectionName = @"Radio message type";
            break;
        case 1 :
            if (self.isReceiveExercise) {
                sectionName = @"Groups number";
            }else{
                sectionName = @"Symbols for training";
            }
            break;
        case 2:
            sectionName = @"Transmission delay";
            break;
        case 3:
        default:
            sectionName = @"Symbols for training";
            break;
    }
    headerView.text= sectionName;
    return  headerView;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.isReceiveExercise) {
        return 4;
    }else
    {
        return 2;
    }
}
@end
