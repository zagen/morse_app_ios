//
//  MorseAppSelectTextFileVC.m
//  MorseApp
//
//  Created by Admin on 03.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "MorseAppSelectTextFileVC.h"

@interface MorseAppSelectTextFileVC ()

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *okButton;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray * filelist;
@property (strong, nonatomic) NSString * docsDir;
@property (strong, nonatomic) NSString * filename;
@property (strong, nonatomic) NSTimer * tapTimer;
@property (nonatomic) int tapCount;
@property (nonatomic) int tappedRow;
@end

@implementation MorseAppSelectTextFileVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.backButton.layer.cornerRadius = self.backButton.frame.size.width/2;
    self.okButton.layer.cornerRadius = self.okButton.frame.size.width/2;
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    
    NSFileManager *filemgr;
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    self.docsDir = dirPaths[0];

    filemgr =[NSFileManager defaultManager];
    NSArray* allFiles = [filemgr contentsOfDirectoryAtPath:self.docsDir error:NULL];
    NSMutableArray * textFilesOnly = [[NSMutableArray alloc]init];
    for (NSString *filename in allFiles) {
        if ([[filename pathExtension] isEqualToString: @"txt"]) {
            [textFilesOnly addObject:filename];
        }
    }
    self.filelist = textFilesOnly;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backToPreviousVC:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.filelist count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [self.tableView dequeueReusableCellWithIdentifier:@"txtFileTableCellIdentifier"];
    cell.textLabel.text = self.filelist[indexPath.row];
    return cell;
}


- (IBAction)okClicked:(id)sender {
    
    if (self.filename) {
        if (self.delegate) {
            [self.delegate selectedTextFileWithName:self.filename];
        }

        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.filename = self.filelist[indexPath.row];
    //checking for double taps here
    if(self.tapCount == 1 && self.tapTimer != nil && self.tappedRow == indexPath.row){
        
        [self.tapTimer invalidate];
        [self setTapTimer:nil];
        
        [self okClicked:nil];
        
    }
    else if(self.tapCount == 0){
        //This is the first tap. If there is no tap till tapTimer is fired, it is a single tap
        self.tapCount = self.tapCount + 1;
        self.tappedRow = indexPath.row;
        [self setTapTimer:[NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(tapTimerFired:) userInfo:nil repeats:NO]];
    }
    else if(self.tappedRow != indexPath.row){
        //tap on new row
        self.tapCount = 0;
        if(self.tapTimer != nil){
            [self.tapTimer invalidate];
            [self setTapTimer:nil];
        }
    }
}

- (void)tapTimerFired:(NSTimer *)aTimer{
    //timer fired, there was a single tap on indexPath.row = tappedRow
    if(self.tapTimer != nil){
        self.tapCount = 0;
        self.tappedRow = -1;
    }
}


@end
