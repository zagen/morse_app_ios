//
//  MorseAppResultVC.h
//  MorseApp
//
//  Created by Admin on 19.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MorseAppResultVC : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) NSArray* resultsData;
@property (nonatomic) BOOL returnToPreviousViewController;
@end
