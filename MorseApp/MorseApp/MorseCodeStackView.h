//
//  MorseCodeStackView.h
//  MorseApp
//
//  Created by Admin on 16.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MorseCodeStackView : UIView
@property (nonatomic)int symbolSize;
-(void)addMorseCode:(NSString*) morseCode;
-(void)clear;
-(NSString *)stringValue;
@end
