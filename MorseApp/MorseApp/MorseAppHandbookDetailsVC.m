//
//  MorseAppHandbookDetailsVC.m
//  MorseApp
//
//  Created by Admin on 22.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "MorseAppHandbookDetailsVC.h"
#import "MorseCodeStackView.h"
#import "General/Transmitter.h"
#import "General/UILabel+ShowPopup.h"

@interface MorseAppHandbookDetailsVC ()
@property (weak, nonatomic) IBOutlet UILabel *characterNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *characterLabel;

@property (weak, nonatomic) IBOutlet MorseCodeStackView *morseCodeView;
@property (weak, nonatomic) IBOutlet UILabel *mnemonicLabel;
@property (weak, nonatomic) IBOutlet UIButton *returnButton;
@property (weak, nonatomic) IBOutlet UILabel *mnemonicTitleLabel;
@property (strong, nonatomic) Transmitter * transmitter;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@end

@implementation MorseAppHandbookDetailsVC


-(Transmitter *)transmitter
{
    if (!_transmitter) {
        _transmitter = [Transmitter getCurrentTransmitter];
    }
    return _transmitter;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.morseCodeView.symbolSize = 7;
    self.returnButton.layer.cornerRadius = self.returnButton.frame.size.width/2;
    [self.statusLabel showPopup:@"For playing code of character just touch it"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self updateUI];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateUI
{
    if (self.handbook) {
        self.characterLabel.text = [self.handbook characterAtIndex:self.indexOfDisplayedCharacter];
        self.characterNameLabel.text = [self.handbook characterNameAtIndex:self.indexOfDisplayedCharacter];
        self.mnemonicLabel.text = [self.handbook mnemonicAtIndex:self.indexOfDisplayedCharacter];
        [self.morseCodeView clear];
        [self.morseCodeView addMorseCode:[self.handbook morseCodeOfCharacterWithIndex:self.indexOfDisplayedCharacter]];
        NSLog(@"mnemonic : %@", [self.handbook mnemonicAtIndex:self.indexOfDisplayedCharacter]);
    }
}
- (IBAction)return:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)swipeRight:(UISwipeGestureRecognizer *)sender {
    
    self.indexOfDisplayedCharacter = (self.indexOfDisplayedCharacter != 0)? self.indexOfDisplayedCharacter - 1 : 0;
    double animationDuration = 0.5;
    [UIView transitionWithView:self.characterLabel
                      duration:animationDuration
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        self.characterLabel.text = [self.handbook characterAtIndex:self.indexOfDisplayedCharacter];
                    }
                    completion:^(BOOL completed){
                    }];
    [UIView animateWithDuration:animationDuration/2
                     animations:^{
                         self.morseCodeView.alpha = 0;
                         self.characterNameLabel.alpha = 0;
                         self.mnemonicLabel.alpha = 0;
                         self.mnemonicTitleLabel.alpha = 0;
                     }
                     completion:^(BOOL finished){
                         self.characterNameLabel.text = [self.handbook characterNameAtIndex:self.indexOfDisplayedCharacter];
                         [self.morseCodeView clear];
                         [self.morseCodeView addMorseCode:[self.handbook morseCodeOfCharacterWithIndex:self.indexOfDisplayedCharacter]];
                         self.mnemonicLabel.text = [self.handbook mnemonicAtIndex:self.indexOfDisplayedCharacter];
                         
                         [UIView animateWithDuration:animationDuration/2
                                          animations:^{
                                              self.morseCodeView.alpha = 1;
                                              self.characterNameLabel.alpha = 1;
                                              self.mnemonicLabel.alpha = 1;
                                              self.mnemonicTitleLabel.alpha = 1;
                                          }completion:nil];
                     }];

}
- (IBAction)swipeLeft:(UISwipeGestureRecognizer *)sender {
    self.indexOfDisplayedCharacter = (self.indexOfDisplayedCharacter == [self.handbook charactersCount] - 1)? self.indexOfDisplayedCharacter : self.indexOfDisplayedCharacter + 1;
    double animationDuration = 0.5;
    [UIView transitionWithView:self.characterLabel
                      duration:animationDuration
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    animations:^{
                        self.characterLabel.text = [self.handbook characterAtIndex:self.indexOfDisplayedCharacter];
                    }
                    completion:^(BOOL completed){
                    }];
    [UIView animateWithDuration:animationDuration/2
                     animations:^{
                         self.morseCodeView.alpha = 0;
                         self.characterNameLabel.alpha = 0;
                         self.mnemonicLabel.alpha = 0;
                         self.mnemonicTitleLabel.alpha = 0;
                     }
                     completion:^(BOOL finished){
                         [self.morseCodeView clear];
                         [self.morseCodeView addMorseCode:[self.handbook morseCodeOfCharacterWithIndex:self.indexOfDisplayedCharacter]];
                         self.characterNameLabel.text = [self.handbook characterNameAtIndex:self.indexOfDisplayedCharacter];
                         self.mnemonicLabel.text = [self.handbook mnemonicAtIndex:self.indexOfDisplayedCharacter];
                         
                         [UIView animateWithDuration:animationDuration/2
                                          animations:^{
                                              self.morseCodeView.alpha = 1;
                                              self.characterNameLabel.alpha = 1;
                                              self.mnemonicLabel.alpha = 1;
                                              self.mnemonicTitleLabel.alpha = 1;
                                          }completion:nil];
                     }];

}
- (IBAction)playMorseCode:(id)sender {
    [self.transmitter transmitt:self.characterLabel.text];
    [self.statusLabel showPopup:[NSString stringWithFormat:@"Playing Morse code for character: %@", self.characterLabel.text]];
}

@end
