//
//  Settings.h
//  MorseApp
//
//  Created by Admin on 21.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const SETTINGS_LOCALE_INT;
extern NSString * const SETTINGS_TRANSMITTER_INT;
extern NSString * const SETTINGS_ANIMATION_HELP_ON_INT;
extern NSString * const SETTINGS_TRANSMISSION_SPEED_INT;
extern NSString * const SETTINGS_VOLUME_INT;
extern NSString * const SETTINGS_GENERATOR_SIZE_INT;
extern NSString * const SETTINGS_GROUPS_NUMBER_INT;
extern NSString * const SETTINGS_TONE_FREQUENCE_INT;
extern NSString * const SETTINGS_PERSONAL_RECORD_INT;
extern NSString * const SETTINGS_ACHIEVEMENTS_ARRAY_OF_NAMES;
extern NSString * const SETTINGS_GENERATOR_SEQUENCE_TYPE_INT;
extern NSString * const SETTINGS_TEXT_FILENAME_STRING ;
extern NSString * const SETTINGS_TEXT_CURRENTPOSITION_INT;
extern NSString * const SETTINGS_GENERATOR_CURRENT_ABC_INTER_STRING;
extern NSString * const SETTINGS_GENERATOR_CURRENT_ABC_RUS_STRING;
extern NSString * const SETTINGS_LISTENING_TRANSMISSION_DELAY_INT;


extern int const SETTINGS_DEFAULT_LOCALE_INT;
extern int const SETTINGS_DEFAULT_TRANSMITTER_INT;
extern int const SETTINGS_DEFAULT_ANIMATION_HELP_ON_INT;
extern int const SETTINGS_DEFAULT_TRANSMISSION_SPEED_INT;
extern int const SETTINGS_DEFAULT_VOLUME_INT;
extern int const SETTINGS_DEFAULT_GENERATOR_SIZE_INT;
extern int const SETTINGS_DEFAULT_GROUPS_NUMBER_INT;
extern int const SETTINGS_DEFAULT_TONE_FREQUENCE_INT;
extern int const SETTINGS_DEFAULT_PERSONAL_RECORD_INT;
extern NSArray* const SETTINGS_DEFAULT_ACHIEVEMENTS_ARRAY_OF_NAMES;
extern int const SETTINGS_DEFAULT_GENERATOR_SEQUENCE_TYPE_INT;
extern NSString * const SETTINGS_DEFAULT_TEXT_FILENAME_STRING;
extern int const SETTINGS_DEFAULT_TEXT_CURRENTPOSITION_INT;
extern int const SETTINGS_DEFAULT_LISTENING_TRANSMISSION_DELAY_INT;

extern NSString * const SETTINGS_ABC_LATIN;
extern NSString * const SETTINGS_ABC_CYRILLYC;

@interface Settings : NSObject
@property (nonatomic) int locale;
@property (nonatomic) int transmitter;
@property (nonatomic) int transmitterSpeed;
@property (nonatomic) int isAnimatedHelpOn;
@property (nonatomic) int volume;
@property (nonatomic) int generatorSize;
@property (nonatomic) int groupsNumber;
@property (nonatomic) int toneFrequence;
@property (nonatomic) int personalRecord;
@property (strong, nonatomic) NSArray * achievements;
@property (nonatomic) int sequenceType;
@property (strong ,nonatomic) NSString * textFilename;
@property (nonatomic) int textCurrentPosition;
@property (strong, nonatomic) NSString * currentABCInt;
@property (strong, nonatomic) NSString * currentABCRus;
@property (nonatomic) int delayBeforeTransmission;

-(void)save;
-(void)restore;
-(void)resetToDefaults;
+(id)sharedSettings;
@end
