//
//  MorseAppMainMenuVC.m
//  MorseApp
//
//  Created by Admin on 13.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "MorseAppMainMenuVC.h"
#import "Settings.h"

@interface MorseAppMainMenuVC ()
@property (strong, nonatomic) UIButton * animatedHintButton;
@property (strong, nonatomic) Settings * settings;
@property (nonatomic) int currentTag;
@property (strong, nonatomic) NSTimer * disappearTimer;
@end

@implementation MorseAppMainMenuVC

-(Settings *)settings
{
    if (!_settings) {
        _settings = [Settings sharedSettings];
    }
    return _settings;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.animatedHintButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [self.animatedHintButton addTarget:self
               action:@selector(animatedHintButtonPressed:)
     forControlEvents:UIControlEventTouchUpInside];

    self.animatedHintButton.frame = CGRectMake(10 ,
                                               -50,
                                               self.view.bounds.size.width - 20 ,
                                               50);
    self.animatedHintButton.alpha = 0;
    self.animatedHintButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.animatedHintButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.animatedHintButton.tintColor = [UIColor blackColor];
    self.animatedHintButton.backgroundColor = [UIColor whiteColor];
    self.animatedHintButton.layer.cornerRadius = self.animatedHintButton.bounds.size.height/4;

    [self.view addSubview:self.animatedHintButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) animatedHintButtonPressed:(id)sender
{
    [self segue];
}
-(NSAttributedString *)getButtonTitleByTag:(int) tag
{
    NSString * mainTitle;
    switch (tag) {
        case 1:
            mainTitle = @"Transmit training";
            break;
            
        case 2:
            mainTitle = @"Receive training";
            break;
        case 3:
            mainTitle = @"Handbook";
            break;
        case 4:
            mainTitle = @"Bonus";
            break;
        case 5:
            mainTitle = @"Achievements";
            break;
        case 6:
        default:
            mainTitle = @"Settings";
            break;
    }
    NSMutableAttributedString * title = [[NSMutableAttributedString alloc] initWithString:mainTitle
                                                                 attributes:@{
                                                                              NSForegroundColorAttributeName : [UIColor blackColor],
                                                                              NSFontAttributeName : [UIFont systemFontOfSize:24]
                                                                              }];
    [title appendAttributedString:[[NSAttributedString alloc]initWithString:@"\nTap to go!" attributes:@{
                                                                                                         NSForegroundColorAttributeName : [UIColor blackColor],
                                                                                                         NSFontAttributeName : [UIFont systemFontOfSize:9]
                                                                                                        }]];
    return title;
}

- (IBAction)buttonClicked:(id)sender {
    self.currentTag = ((UIButton *)sender).tag;
    if (self.settings.isAnimatedHelpOn) {
        //remove all animation
        
        [self.animatedHintButton.layer removeAllAnimations];
        
        //set correct title for animated help
        [self.animatedHintButton setAttributedTitle:[self getButtonTitleByTag:self.currentTag] forState:UIControlStateNormal];
        
        //set button (animated help) on top of central view and set it transparent
        CGRect frame = self.animatedHintButton.frame;
        frame.origin.y = frame.size.height * -1;
        self.animatedHintButton.frame = frame;
        self.animatedHintButton.alpha = 0;

        //if button already on screen reset timer before button will start disappear
        [self.disappearTimer invalidate];
        
        //animation
        [UIView animateWithDuration:0.5
                              delay:0
                            options: UIViewAnimationCurveEaseIn
                         animations:^
         {
             CGRect frame = self.animatedHintButton.frame;
             frame.origin.y = self.view.center.y - frame.size.height/2;
             self.animatedHintButton.frame = frame;
             self.animatedHintButton.alpha = 1;
         }
                         completion:^(BOOL finished)
         {
             self.disappearTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                                                    target:self
                                                                  selector:@selector(getLostAnimatedHelp:)
                                                                  userInfo:nil
                                                                   repeats:NO];
             
         }];
    }else{
        [self segue];
    }

}
-(void)getLostAnimatedHelp:(NSTimer*)timer
{
    [UIView animateWithDuration:0.5
                          delay:0
                        options: UIViewAnimationCurveEaseIn
                     animations:^
     {
         CGRect frame = self.animatedHintButton.frame;
         frame.origin.y = self.view.frame.size.height;
         
         self.animatedHintButton.frame = frame;
         self.animatedHintButton.alpha = 0;
     }
                     completion:^(BOOL finished)
     {
         CGRect frame = self.animatedHintButton.frame;
         frame.origin.y = -1 * frame.size.height;
         
         self.animatedHintButton.frame = frame;
         self.animatedHintButton.alpha = 0;
     }];

}

-(void)segue
{
    NSString * identifier;
    switch (self.currentTag) {
        case 1:
            identifier = @"mainMenuToTransmitExerciseSegue";
            break;
            
        case 2:
            identifier = @"mainMenuToReceiveExerciseSegue";
            break;
        case 3:
            identifier = @"mainMenuToHandbookListSegue";
            break;
        case 4:
            identifier = @"mainMenuToBonusVCSegue";
            break;
        case 5:
            identifier = @"mainMenuToAchievementsSegue";
            break;
        case 6:
        default:
            identifier = @"mainMenuToSettingsSegue";
            break;

    }
    [self performSegueWithIdentifier:identifier sender:self];
}
@end
