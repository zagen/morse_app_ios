//
//  HandbookTableCell.m
//  MorseApp
//
//  Created by Admin on 22.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "HandbookTableCell.h"
#import "MorseCodeStackView.h"
#import "TextCodec.h"
#import "Settings.h"

@interface HandbookTableCell()

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet MorseCodeStackView *morseCodeStackView;
@property (strong, nonatomic) TextCodec * codec;
@property (strong, nonatomic) Settings * settings;
@end;

@implementation HandbookTableCell

@synthesize text = _text;

-(TextCodec *)codec
{
    if (!_codec) {
        _codec = [[TextCodec alloc]init];
        _codec.locale = (self.settings.locale)? RUS : INT;
    }
    return _codec;
}

-(Settings *)settings
{
    if (!_settings) {
        _settings = [Settings sharedSettings];
    }
    return _settings;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)setText:(NSString *)text
{
    self.label.text = text;
    [self.codec setText:text];
    [self.morseCodeStackView clear];
    [self.morseCodeStackView addMorseCode:self.codec.encodedText];
}
@end
