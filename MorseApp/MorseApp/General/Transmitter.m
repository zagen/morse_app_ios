//
//  Transmitter.m
//  MorseApp
//
//  Created by Admin on 29.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "Transmitter.h"
#import "ToneTransmitter.h"
#import "Settings.h"

@interface Transmitter()
@property (strong,nonatomic, readwrite) TextCodec * codec;

@end
@implementation Transmitter
-(TextCodec *)codec
{
    if (!_codec) {
        _codec = [[TextCodec alloc]init];
    }
    return _codec;
}
-(void) transmitt:(NSString *)message
{
    
}
-(void)stop
{
    
}
-(void) transmitt
{
    
}
-(void) replay
{
    
}
+(Transmitter *)getCurrentTransmitter
{
    Settings * settings = [Settings sharedSettings];
    int number = settings.transmitter;
    switch (number) {
        case 0:
            return [[ToneTransmitter alloc]init];
            break;
            
        default:
            break;
    }
    return nil;
}
@end
