//
//  Statistics.m
//  MorseApp
//
//  Created by Admin on 20.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "Statistics.h"
@interface Statistics()
@property (readwrite, nonatomic) int totalCountOfCharacters;
@property (readwrite, nonatomic) int countOfCorrectInputedCharacters;
@end
@implementation Statistics

-(instancetype)initWithResultData:(NSArray *)resultData
{
    if (self = [super init]) {
        self.totalCountOfCharacters = 0;
        self.countOfCorrectInputedCharacters = 0;
        
        for (id item in resultData) {
            if([item isKindOfClass:[NSDictionary class]])
            {
                NSDictionary * resultDic = item;
                NSArray * correctArray = [resultDic allKeys];
                NSString *correctValue = @"";
                NSString *inputedValue = @"";
                if([correctArray count])
                {
                    correctValue = correctArray[0];
                    inputedValue = resultDic[correctValue];
                }
                //calculate min length
                self.totalCountOfCharacters += (int)[correctValue length];
                int minLength = (int)[correctValue length];

                minLength = (minLength < (int)[inputedValue length]) ? minLength : (int)[inputedValue length];
                
                //now compare to string for equal characters
                for (int i = 0; i < minLength; i++) {
                    if([correctValue characterAtIndex: i] == [inputedValue characterAtIndex:i])
                    {
                        self.countOfCorrectInputedCharacters++;
                    }
                }
                
            }
        }
    }
    return  self;
}
-(double)percentageOfCorrectness
{
    if(self.totalCountOfCharacters)
    {
        return (double)(self.countOfCorrectInputedCharacters) / (double)(self.totalCountOfCharacters)*100;
    }
    return 0;
}
@end
