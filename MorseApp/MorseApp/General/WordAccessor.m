//
//  WordAccessor.m
//  MorseApp
//
//  Created by Admin on 14.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "WordAccessor.h"

@interface WordAccessor()
@property (nonatomic) NSArray *words;

@end
@implementation WordAccessor

-(id)init
{
    if(self = [super init]){
        self.locale = INT;
    }
    return self;
}
-(void)setLocale:(enum LOCALE)locale
{
    NSString * filename;
    if(locale == RUS)
    {
        filename = @"rus";
    }else{
        filename = @"eng";
    }
    NSString * path = [[NSBundle mainBundle] pathForResource:filename ofType:@"txt"];
    self.words = [[NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL] componentsSeparatedByString:@"\n"];

}

-(NSArray *)words
{
    if(!_words){
        self.locale = (self.locale) ? self.locale : INT;
    }
    return _words;
}
-(NSString*)randomWord
{
   return [self.words objectAtIndex:arc4random_uniform((int)[self.words count])];;
}

-(NSString *)wordByIndex:(int)index
{
     return [self.words[index] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
}

+(id)sharedWordAccessor
{
    static WordAccessor *sharedWordAccessor = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedWordAccessor = [[self alloc] init];
    });
    return sharedWordAccessor;
}

@end
