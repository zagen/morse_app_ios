//
//  ToneTransmitter.h
//  MorseApp
//
//  Created by Admin on 29.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "Transmitter.h"
#import <AudioUnit/AudioUnit.h>

@interface ToneTransmitter : Transmitter{
    AudioComponentInstance toneUnit;
    @public
        double frequency;
        double sampleRate;
        double theta;
}
@property (nonatomic, strong) NSString * morseCodeString;
@property (nonatomic) int currentCharIndex;
@property (nonatomic) int dotSampleLength;
@property (nonatomic) int dashSampleLength;
@property (nonatomic) int spaceBetweenMorseSampleLength;
@property (nonatomic) int spaceBetweenCharsSampleLength;
@property (nonatomic) int spaceBetweenWordsSampleLength;
@property (nonatomic) int currentSampleIndex;
@property (nonatomic) int currentSampleLength;
@property (nonatomic) BOOL isSignal;
@property (nonatomic) double fadePercent;


- (void)seekNextChar;
@end
