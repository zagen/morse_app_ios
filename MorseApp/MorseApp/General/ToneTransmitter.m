//
//  ToneTransmitter.m
//  MorseApp
//
//  Created by Admin on 29.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "ToneTransmitter.h"
#import <AVFoundation/AVAudioSession.h>
#import "Settings.h"

OSStatus RenderTone(
                    void *inRefCon,
                    AudioUnitRenderActionFlags 	*ioActionFlags,
                    const AudioTimeStamp 		*inTimeStamp,
                    UInt32 						inBusNumber,
                    UInt32 						inNumberFrames,
                    AudioBufferList 			*ioData)

{
	// Fixed amplitude is good enough for our purposes
	const double amplitude = 0.8;
    
	// Get the tone parameters out of the view controller
	ToneTransmitter *toneTransmitter =
    (__bridge ToneTransmitter *)inRefCon;
	double theta = toneTransmitter->theta;
	double theta_increment = 2.0 * M_PI * toneTransmitter->frequency / toneTransmitter->sampleRate;
    
	// This is a mono tone generator so we only need the first buffer
	const int channel = 0;
	Float32 *buffer = (Float32 *)ioData->mBuffers[channel].mData;
	
	// Generate the samples
	for (UInt32 frame = 0; frame < inNumberFrames; frame++)
	{
        if (toneTransmitter.isSignal) {
            double fadeFactor = 1;
            if (toneTransmitter.currentSampleIndex < toneTransmitter.currentSampleLength * toneTransmitter.fadePercent) {
                fadeFactor = toneTransmitter.currentSampleIndex / (toneTransmitter.currentSampleLength * toneTransmitter.fadePercent);
            }else if (toneTransmitter.currentSampleIndex > toneTransmitter.currentSampleLength * (1 - toneTransmitter.fadePercent))
            {
                fadeFactor = (toneTransmitter.currentSampleLength - toneTransmitter.currentSampleIndex)/ (toneTransmitter.currentSampleLength * toneTransmitter.fadePercent);
            }
            buffer[frame] = sin(theta) * amplitude * fadeFactor;
        }else{
            buffer[frame] = 0;
        }
        toneTransmitter.currentSampleIndex++;
        
		theta += theta_increment;
		if (theta > 2.0 * M_PI)
		{
			theta -= 2.0 * M_PI;
		}
        
        if(toneTransmitter.currentSampleIndex >= toneTransmitter.currentSampleLength)
        {
            if (toneTransmitter.isSignal) {
                toneTransmitter.isSignal = NO;
                toneTransmitter.currentSampleIndex = 0;
                toneTransmitter.currentSampleLength = toneTransmitter.spaceBetweenMorseSampleLength;
                NSLog(@"Space between morse %i", toneTransmitter.currentSampleLength);
            }else{
                [toneTransmitter seekNextChar];
            }
        }
	}
	
	// Store the theta back in the view controller
	toneTransmitter->theta = theta;
    
	return noErr;
}

@interface ToneTransmitter()
@property (strong,nonatomic) Settings * settings;
@property (nonatomic) enum LOCALE locale;
@end
@implementation ToneTransmitter

-(Settings *)settings
{
    if (!_settings) {
        _settings = [Settings sharedSettings];
    }
    return _settings;
}

-(void)seekNextChar
{
    ++self.currentCharIndex;
    self.currentSampleIndex = 0;
    self.isSignal = YES;
    
    
    if (self.currentCharIndex >= [self.morseCodeString length]) {
        [self performSelectorOnMainThread:@selector(stop) withObject:nil waitUntilDone:NO];
    }
    unichar currentChar = [self.morseCodeString characterAtIndex:self.currentCharIndex];
    if (currentChar == '.'){
        self.currentSampleLength = self.dotSampleLength;
    }else if(currentChar == '-')
    {
        self.currentSampleLength = self.dashSampleLength;
    }else if(currentChar == ' ')
    {
        self.currentSampleLength = self.spaceBetweenCharsSampleLength;
        self.isSignal = NO;
    }else
    {
        self.currentSampleLength = self.spaceBetweenWordsSampleLength;
        self.isSignal = NO;
    }
    
    NSLog(@"Seek next %@", [NSString stringWithCharacters:&currentChar length:1]);
    NSLog(@"Current index %i", self.currentCharIndex);
    NSLog(@"Sample length %i", self.currentSampleLength);
}

- (void)createToneUnit
{
	// Configure the search parameters to find the default playback output unit
	// (called the kAudioUnitSubType_RemoteIO on iOS but
	// kAudioUnitSubType_DefaultOutput on Mac OS X)
	AudioComponentDescription defaultOutputDescription;
	defaultOutputDescription.componentType = kAudioUnitType_Output;
	defaultOutputDescription.componentSubType = kAudioUnitSubType_RemoteIO;
	defaultOutputDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
	defaultOutputDescription.componentFlags = 0;
	defaultOutputDescription.componentFlagsMask = 0;
	
	// Get the default playback output unit
	AudioComponent defaultOutput = AudioComponentFindNext(NULL, &defaultOutputDescription);
	NSAssert(defaultOutput, @"Can't find default output");
	
	// Create a new unit based on this that we'll use for output
	OSErr err = AudioComponentInstanceNew(defaultOutput, &toneUnit);
	NSAssert1(toneUnit, @"Error creating unit: %hd", err);
	
	// Set our tone rendering function on the unit
	AURenderCallbackStruct input;
	input.inputProc = RenderTone;
	input.inputProcRefCon = (__bridge void *)(self);
	err = AudioUnitSetProperty(toneUnit,
                               kAudioUnitProperty_SetRenderCallback,
                               kAudioUnitScope_Input,
                               0,
                               &input,
                               sizeof(input));
	NSAssert1(err == noErr, @"Error setting callback: %hd", err);
	
	// Set the format to 32 bit, single channel, floating point, linear PCM
	const int four_bytes_per_float = 4;
	const int eight_bits_per_byte = 8;
	AudioStreamBasicDescription streamFormat;
	streamFormat.mSampleRate = sampleRate;
	streamFormat.mFormatID = kAudioFormatLinearPCM;
	streamFormat.mFormatFlags =
    kAudioFormatFlagsNativeFloatPacked | kAudioFormatFlagIsNonInterleaved;
	streamFormat.mBytesPerPacket = four_bytes_per_float;
	streamFormat.mFramesPerPacket = 1;
	streamFormat.mBytesPerFrame = four_bytes_per_float;
	streamFormat.mChannelsPerFrame = 1;
	streamFormat.mBitsPerChannel = four_bytes_per_float * eight_bits_per_byte;
	err = AudioUnitSetProperty (toneUnit,
                                kAudioUnitProperty_StreamFormat,
                                kAudioUnitScope_Input,
                                0,
                                &streamFormat,
                                sizeof(AudioStreamBasicDescription));
	NSAssert1(err == noErr, @"Error setting stream format: %hd", err);
}
-(void)stop
{
    if (toneUnit)
	{
		AudioOutputUnitStop(toneUnit);
		AudioUnitUninitialize(toneUnit);
		AudioComponentInstanceDispose(toneUnit);
		toneUnit = nil;
        if (self.delegate) {
            [self.delegate transmittanceFinished];
        }
    }
    self.settings.locale = self.locale;

}
-(void)transmitt
{
    [self transmitt:self.text];
}

-(void)transmitt:(NSString *)message
{
    self.locale = self.settings.locale;
    self.settings.locale = ALL;
    self.text = message;
    [self.codec setText:self.text];
    self.morseCodeString = self.codec.encodedText;
    
    if (self.morseCodeString && [self.morseCodeString length]) {

        if (!toneUnit) {
            [self createToneUnit];
            self.currentCharIndex =  -1;
            [self seekNextChar];
            // Stop changing parameters on the unit
            OSErr err = AudioUnitInitialize(toneUnit);
            NSAssert1(err == noErr, @"Error initializing unit: %hd", err);
            
            // Start playback
            err = AudioOutputUnitStart(toneUnit);
            NSAssert1(err == noErr, @"Error starting unit: %hd", err);
        }

    }
    

}

-(instancetype)init
{
    if (self = [super init]) {
        frequency = self.settings.toneFrequence;
        sampleRate = 44100;
        
        NSError *audioSessionError = nil;
        AVAudioSession *session = [AVAudioSession sharedInstance];     // 1
        
        [session setCategory: AVAudioSessionCategoryAmbient      // 3
                         error: &audioSessionError];
        [session setActive: YES
                       error: &audioSessionError];
        [session setPreferredSampleRate:sampleRate error:nil];
        sampleRate = [session sampleRate];
        int WPM = self.settings.transmitterSpeed;
        double duration = ceil(1200.0/WPM );
        NSLog(@"Duration of 1 dot ms : %f", duration);
        NSLog(@"Samples num of 1 dot: %i", self.dotSampleLength);
        self.dotSampleLength = ceil(duration * sampleRate /1000);
        NSLog(@"Samples rate: %f", sampleRate);
        
        self.dashSampleLength = self.dotSampleLength * 3;
        self.spaceBetweenMorseSampleLength = self.dotSampleLength;
        self.spaceBetweenCharsSampleLength = self.dotSampleLength * 2;
        self.spaceBetweenWordsSampleLength = self.dotSampleLength * 6;
        self.isSignal = YES;
        self.fadePercent = 0.15;
        

    }
    return self;
}
-(void)replay{
    self.currentCharIndex = -1;
    [self seekNextChar];
}
@end
