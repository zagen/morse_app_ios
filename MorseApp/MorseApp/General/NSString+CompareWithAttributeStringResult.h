//
//  NSString+CompareWithAttributeStringResult.h
//  MorseApp
//
//  Created by Admin on 07.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CompareWithAttributeStringResult)
-(NSAttributedString *)highlightDifferentFromString:(NSString *)correctString correctColor:(UIColor *)correctColor wrongColor: (UIColor *) wrongColor andSize:(float)textSize;
@end
