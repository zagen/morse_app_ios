//
//  TextFileWordReader.m
//  MorseApp
//
//  Created by Admin on 04.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "TextFileWordReader.h"
#import "../Settings.h"
#import "../UniversalDetector/UniversalDetector.h"

@interface TextFileWordReader()
@property (strong, nonatomic) NSString * documentsPath;
@property (strong,nonatomic) Settings *settings;
@property (strong, nonatomic) NSString * textbuffer;
@property (nonatomic) int currentPosition;


@end
@implementation TextFileWordReader

-(instancetype)init
{
    if (self = [super init]) {
        self.filename = self.settings.textFilename;
        self.currentPosition = self.settings.textCurrentPosition;
        self.percent = [self.textbuffer length] ? (float)self.currentPosition / ([self.textbuffer length] - 1) * 100 : 0;
    }
    return self;
}
-(NSString *)documentsPath
{
    if (!_documentsPath) {
        _documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    }
    return _documentsPath;
}
-(BOOL)filenameExist:(NSString *)filename{
    
    NSString * fullPath = [self.documentsPath stringByAppendingPathComponent:filename];
    return [[NSFileManager defaultManager] fileExistsAtPath:fullPath];
}
-(Settings *)settings
{
    if (!_settings) {
        _settings = [Settings sharedSettings];
    }
    return _settings;
}
-(BOOL)hasFile
{
    if (self.filename && [self filenameExist:self.filename]) {
        return YES;
    }
    return NO;
}
-(void)setFilename:(NSString *)filename
{
    if ([self filenameExist:filename]) {
        NSString * fullPath = [self.documentsPath stringByAppendingPathComponent:filename];

        NSStringEncoding encoding;
        self.textbuffer = [NSString stringWithContentsOfFile:fullPath usedEncoding:&encoding error:nil];
        if (!self.textbuffer) {
            NSData * data = [NSData dataWithContentsOfFile:fullPath];
            NSString * encodingName = [UniversalDetector encodingAsStringWithData:data];
            NSLog(@"Encoding : %@", encodingName);
            if ([encodingName isEqualToString:@"windows-1251"]) {
                
                self.textbuffer = [[NSString alloc]initWithData:data encoding:NSWindowsCP1251StringEncoding];
            }else if([encodingName isEqualToString:@"KOI8-R"])
            {
                self.textbuffer = [[NSString alloc]initWithData:data encoding:CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingKOI8_R)];
            }
            

        }
        if (self.textbuffer) {
            _filename = filename;
        }else
        {
            _filename = nil;
        }
        
        self.percent = 0;
        self.currentPosition = 0;
        if (![self.settings.textFilename isEqualToString:filename]) {
            self.settings.textCurrentPosition = 0;
        }
        self.settings.textFilename = filename;
        [self.settings save];
        NSLog(@"Chars counts : %i\nEncoding : %i", [self.textbuffer length], encoding);
    }
}
-(NSString *)nextWord
{
    int index = self.currentPosition;
    
    while ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:[self.textbuffer characterAtIndex:index]] && index < [self.textbuffer length] - 1)
    {
        index++;
    }
    
    NSRange range = [self.textbuffer rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] options:nil range:NSMakeRange(index, [self.textbuffer length]- index)];
    if (range.location != NSNotFound) {
        NSString * result =  [self.textbuffer substringWithRange:NSMakeRange(index, range.location - index)];
        result = [result stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (![result length]) {
            result = nil;
        }
        return [result stringByAppendingString:@" "];
    }
    
    return nil;
}
-(NSString *)previousWord
{
    int index = self.currentPosition;
    
    while ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:[self.textbuffer characterAtIndex:index]] && index > 0)
    {
        index--;
    }
    
    NSRange range = [self.textbuffer rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] options:NSBackwardsSearch range:NSMakeRange(0, index)];
    if (range.location != NSNotFound) {
        NSString *result = [self.textbuffer substringWithRange:NSMakeRange(range.location, index - range.location + 1)];
        result = [result stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (![result length]) {
            result = nil;
        }
        return [result stringByAppendingString:@" "];;
    }
    
    return nil;
}
-(void)seekForwardToFirstNonSpaceChar
{
    while ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:[self.textbuffer characterAtIndex:self.currentPosition]] && self.currentPosition < [self.textbuffer length] - 1)
    {
        self.currentPosition++;
    }
    self.percent = [self.textbuffer length] ? (float)self.currentPosition / ([self.textbuffer length] - 1) * 100 : 0;

}
-(void) seekToNextWord
{
    [self seekForwardToFirstNonSpaceChar];
    while (![[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:[self.textbuffer characterAtIndex:self.currentPosition]] && self.currentPosition < [self.textbuffer length] - 1)
    {
        self.currentPosition++;
    }
    self.percent = [self.textbuffer length] ? (float)self.currentPosition / ([self.textbuffer length] - 1) * 100 : 0;

}
-(void)seekBackwardToFirstNonSpaceChar
{
    while ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:[self.textbuffer characterAtIndex:self.currentPosition]] && self.currentPosition > 0)
    {
        self.currentPosition--;
    }
    self.percent = [self.textbuffer length] ? (float)self.currentPosition / ([self.textbuffer length] - 1) * 100 : 0;

}
-(void) seekToPreviousWord
{
    [self seekBackwardToFirstNonSpaceChar];
    while (![[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:[self.textbuffer characterAtIndex:self.currentPosition]] && self.currentPosition > 0)
    {
        self.currentPosition--;
    }
    self.percent = [self.textbuffer length] ? (float)self.currentPosition / ([self.textbuffer length] - 1) * 100 : 0;
}
-(void) seekToNextTenWord
{
    for (int i = 0; i < 10; i++) {
        if ([self nextWord]) {
            [self seekToNextWord];
        }else
            break;
    }
    self.percent = [self.textbuffer length] ? (float)self.currentPosition / ([self.textbuffer length] -1) * 100 : 0;

}
-(void) seekToPreviousTenWord
{
    for (int i = 0; i < 10; i++) {
        if ([self previousWord]) {
            [self seekToPreviousWord];
        }else
            break;
    }
    self.percent = [self.textbuffer length] ? (float)self.currentPosition / ([self.textbuffer length] - 1) * 100 : 0;
}
-(void) gotoPercent:(float)percent{
    
    if (percent >= 0 && percent <= 100) {
        self.currentPosition = ([self.textbuffer length] - 1) * (percent/100);
        while (![[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:[self.textbuffer characterAtIndex:self.currentPosition]] && self.currentPosition > 0)
        {
            self.currentPosition--;
        }
        self.percent = [self.textbuffer length] ? (float)self.currentPosition / ([self.textbuffer length] - 1) * 100 : 0;
    }
}
-(void)saveProgress
{
    self.settings.textCurrentPosition = self.currentPosition;
    [self.settings save];
}
@end
