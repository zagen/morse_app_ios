//
//  Generator.h
//  MorseApp
//
//  Created by Admin on 13.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+NSString_containsCategory.h"
#import "WordAccessor.h"



enum GENERATOR_TYPE:NSInteger{
    WORDS = 1,
    CHARS = 0
};

@interface Generator : NSObject
@property (nonatomic) int size;
@property (nonatomic) enum LOCALE local;
@property (nonatomic) NSString * abc;
@property (nonatomic) enum GENERATOR_TYPE sequenceType;

-(NSString *)next;
-(NSString *)nextGroups;
-(id)init;
-(id)initWithSize:(int) size andLocal:(enum LOCALE)local;

@end
