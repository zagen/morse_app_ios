//
//  Generator.m
//  MorseApp
//
//  Created by Admin on 13.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "Generator.h"
#import "Settings.h"

#define GROUP_SIZE 5

@interface Generator()

@property (nonatomic) NSMutableString *sequence;
@property (nonatomic) NSString *abcInt;
@property (nonatomic) NSString *abcRus;
@property (nonatomic) NSArray *groups;
@property (nonatomic) NSArray *currentGroups;
@property (nonatomic) NSArray *groupChances;
@property (nonatomic) WordAccessor *wordAccessor;
@property (nonatomic) int generatingGroupCount;
@property (strong, nonatomic) Settings *settings;

/*
 here need put a word accessor
 */

-(unichar) getRandomCharFromGroup:(int)groupIndex;
-(BOOL) isGroupEmpty:(int)groupIndex;

@end

@implementation Generator

-(id)init
{
    if(self = [super init]){
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(generatorSettingsChanged:)
                                                     name:@"SettingsChangedNotification"
                                                   object:nil];
        [self initFromSettings];

    }
    return self;
}
-(void) initFromSettings{
    self.size = self.settings.generatorSize;
    self.local = (self.settings.locale)? RUS : INT;
    self.wordAccessor = [WordAccessor sharedWordAccessor];
    self.wordAccessor.locale = self.local;
    self.generatingGroupCount = self.settings.groupsNumber;
    self.sequenceType = (self.settings.sequenceType) ? WORDS: CHARS;
    self.abcInt = self.settings.currentABCInt;
    self.abcRus = self.settings.currentABCRus;
}
-(id)initWithSize:(int)size andLocal:(enum LOCALE)local
{
    if(self = [super init]){
        [self initFromSettings];
        self.size = size;
        self.local = local;
        self.wordAccessor.locale = local;


    }
    return self;
}
-(void)generatorSettingsChanged:(NSNotification*)notification
{
    self.size = self.settings.generatorSize;
    self.local = (self.settings.locale)? RUS : INT;
    self.wordAccessor = [WordAccessor sharedWordAccessor];
    self.wordAccessor.locale = self.local;
    self.generatingGroupCount = self.settings.groupsNumber;
    self.sequenceType = (self.settings.sequenceType) ? WORDS: CHARS;
    self.abcInt = self.settings.currentABCInt;
    self.abcRus = self.settings.currentABCRus;
    if (self.local == RUS) {
        self.abc = self.abcRus;
    }else
    {
        self.abc = self.abcInt;
    }

}
-(WordAccessor *)wordAccessor
{
    if (!_wordAccessor) {
        _wordAccessor = [WordAccessor sharedWordAccessor];
    }
    return _wordAccessor;
}

-(Settings *)settings
{
    if (!_settings) {
        _settings = [Settings sharedSettings];
    }
    return _settings;
}
-(NSString *)next
{
    if([self.sequence length] > 0){
        [self.sequence setString: @""];
    }
    if (self.sequenceType == CHARS) {
        if([self.abc length]== 0){
            if(self.local == RUS){
                self.abc = self.abcRus;
            }else{
                self.abc = self.abcInt;
            }
        }
        for (int i = 0; i < self.size; i++) {
            unichar generatedChar = 0;
            //generate random number from 0 to 999;
            int randomNumber = arc4random_uniform(1000);
            for (int j = GROUP_SIZE - 1; j >= 0; j--) {
                
                
                
                //check if random number fit probability of current group
                if(randomNumber < [self.groupChances[j] intValue]){
                    if(![self isGroupEmpty:j]){
                        //if so and current group is not empty, then get random character from group and go out from inner loop
                        generatedChar = [self getRandomCharFromGroup:j];
                        break;
                    }
                }
            }
            //if we don't get no one symbol, then get random character from all abc was set
            if(generatedChar == 0){
                generatedChar = [self.abc characterAtIndex: arc4random_uniform((int)[self.abc length])];
            }
            //and add it to current sequence
            [self.sequence appendString:[NSString stringWithCharacters:&generatedChar length:1]];
        }
    }else{
        return [self.wordAccessor randomWord];
    }

    return [self.sequence uppercaseString];
    
}

-(NSString *)nextGroups
{
    NSMutableString *generatingGroups = [[NSMutableString alloc]init];
    for (int i = 0; i < self.generatingGroupCount; i++) {
        [generatingGroups appendString:[self next]];
        [generatingGroups appendString:@" "];
    }
    self.sequence = [[generatingGroups stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] mutableCopy];
    return self.sequence;
}
-(NSString *)abcInt
{
    if(!_abcInt){
        _abcInt = self.settings.currentABCInt;
    }
    return _abcInt;
}
-(NSString *)abcRus
{
    if (!_abcRus) {
        _abcRus = self.settings.currentABCRus;
    }
    return _abcRus;
}
-(NSArray*)groups
{
    if (!_groups) {
        _groups = [[NSArray alloc]initWithObjects:
        @"оеаитнсрвлкetaonrish",
        @"мдпуяыгзбчйdlfcmugyp",
        @"хъжьюшцщэфёwbvkxjqz",
        @"1234567890",
        @".,:;()\"'-/?!",
        nil];
    }
    return _groups;
}
-(NSArray *)currentGroups
{
    if(!_currentGroups){
        //array with 5 changable empty strings;
        _currentGroups = [[NSArray alloc]initWithObjects:
                          [[NSMutableString alloc]init],
                          [[NSMutableString alloc]init],
                          [[NSMutableString alloc]init],
                          [[NSMutableString alloc]init],
                          [[NSMutableString alloc]init],
                          nil];
    }
    return _currentGroups;
}
-(NSArray *)groupChances
{
    if (!_groupChances) {
        _groupChances = [[NSArray alloc] initWithObjects:
                         [NSNumber numberWithInt:1000],
                         [NSNumber numberWithInt:200],
                         [NSNumber numberWithInt:150],
                         [NSNumber numberWithInt:100],
                         [NSNumber numberWithInt:80],
                         nil];
    }
    return _groupChances;
}

-(NSString *)sequence
{
    if (!_sequence) {
        _sequence = [[NSMutableString alloc] init];
    }
    return _sequence;
}

-(unichar)getRandomCharFromGroup:(int)groupIndex
{

    return [[self.currentGroups objectAtIndex:groupIndex] characterAtIndex:arc4random() %[self.currentGroups[groupIndex] length]];
}
-(BOOL)isGroupEmpty:(int)groupIndex
{
    return [[self.currentGroups objectAtIndex:groupIndex] length] == 0;
}

//setters

-(void)setAbc:(NSString *)abc{
    _abc = abc;
    unichar currentChar;
    NSString *currentCharStr;
    
    //clear current groups
    for(NSMutableString *group in self.currentGroups)
    {
        [group setString:@""];
    }

    for(int i = 0 ; i < [abc length]; i++){
        
        currentChar = [abc characterAtIndex:i];
        currentCharStr = [NSString stringWithCharacters:&currentChar length:1];
        
        for (int j = 0; j < GROUP_SIZE; j++) {
            if ([[self.groups objectAtIndex:j] containsString:currentCharStr]) {
                [self.currentGroups[j] appendString:currentCharStr];
                break;
            }
        }
    }
    
}
-(void)setLocal:(enum LOCALE)local
{
    _local = local;
    self.wordAccessor.locale = local;
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

@end
