//
//  WordAccessor.h
//  MorseApp
//
//  Created by Admin on 14.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TextCodec.h"


@interface WordAccessor : NSObject
@property (nonatomic) enum LOCALE locale;
-(NSString *)randomWord;
-(NSString *)wordByIndex:(int)index;
+(id)sharedWordAccessor;

@end
