//
//  UILabel+ShowPopup.m
//  MorseApp
//
//  Created by Admin on 18.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "UILabel+ShowPopup.h"
#define FADING_TIME 0.5
@interface UILabel()

@property (nonatomic) BOOL finished;
@end

@implementation UILabel (ShowPopup)
-(void)showPopup:(NSString *)message
{
    double showingTime = 0.8;
    [self setText:message];
    self.alpha = 0;
    [UIView animateWithDuration:FADING_TIME animations:^{
            self.alpha = 0.9f;
        }completion:^(BOOL finished) {
            if(finished){
                [NSTimer scheduledTimerWithTimeInterval:showingTime
                                                  target:self
                                                selector: @selector(fadeOut:)
                                                userInfo:nil
                                                 repeats:NO];
                
        }
    }];
}
-(void)fadeOut:(NSTimer *)timer
{
    [UIView animateWithDuration:FADING_TIME animations:^{
        self.alpha = 0.0;
    }];
}
@end
