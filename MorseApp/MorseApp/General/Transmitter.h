//
//  Transmitter.h
//  MorseApp
//
//  Created by Admin on 29.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TextCodec.h"
#import "TransmitterDelegate.h"

@interface Transmitter : NSObject
@property (strong, nonatomic) id<TransmitterDelegate> delegate;
@property (strong, nonatomic, readonly) TextCodec *codec;
@property (strong,nonatomic) NSString * text;

-(void) transmitt:(NSString *)message;
-(void) transmitt;
-(void) replay;
-(void)stop;
+(Transmitter *)getCurrentTransmitter;
@end
