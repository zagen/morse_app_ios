//
//  TransmitterDelegate.h
//  MorseApp
//
//  Created by Admin on 29.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TransmitterDelegate <NSObject>

-(void)transmittanceFinished;

@end
