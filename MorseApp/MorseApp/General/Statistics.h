//
//  Statistics.h
//  MorseApp
//
//  Created by Admin on 20.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Statistics : NSObject
@property (readonly, nonatomic) int totalCountOfCharacters;
@property (readonly, nonatomic) int countOfCorrectInputedCharacters;

-(instancetype)initWithResultData:(NSArray*)resultData;//designated initializer
-(double)percentageOfCorrectness;

@end
