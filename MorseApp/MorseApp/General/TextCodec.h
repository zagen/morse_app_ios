//
//  TextCodec.h
//  MorseApp
//
//  Created by Admin on 11.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <Foundation/Foundation.h>

enum LOCALE:NSInteger{
    ALL = 2,
    RUS = 1,
    INT = 0
};

@interface TextCodec : NSObject
@property(nonatomic) enum LOCALE locale;
@property(nonatomic, readonly) enum LOCALE localeByDefault;
@property(nonatomic, readonly) NSString* encodedText;
@property(nonatomic, readonly) NSString* decodedText;

- (void) setText: (NSString *) text;
- (void) setMorse:(NSString *) morseCode;

@end
