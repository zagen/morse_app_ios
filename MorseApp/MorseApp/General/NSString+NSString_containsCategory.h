//
//  NSString+NSString_containsCategory.h
//  MorseApp
//
//  Created by Admin on 14.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSString_containsCategory)
    - (BOOL) containsString: (NSString*) substring;
@end
