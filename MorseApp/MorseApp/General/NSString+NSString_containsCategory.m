//
//  NSString+NSString_containsCategory.m
//  MorseApp
//
//  Created by Admin on 14.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "NSString+NSString_containsCategory.h"

@implementation NSString (NSString_containsCategory)
- (BOOL) containsString: (NSString*) substring
{
    NSRange range = [self rangeOfString : substring];
    BOOL found = ( range.location != NSNotFound );
    return found;
}
@end
