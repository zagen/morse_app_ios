	//
//  TextCodec.m
//  MorseApp
//
//  Created by Admin on 11.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "TextCodec.h"
#import "Settings.h"

@interface TextCodec()

@property(nonatomic, readwrite) enum LOCALE localeByDefault;
@property(nonatomic, readwrite) NSString* encodedText;
@property(nonatomic, readwrite) NSString* decodedText;
@property(nonatomic) NSDictionary * encMapIntLet;
@property(nonatomic) NSDictionary * encMapRusLet;
@property(nonatomic) NSDictionary * encMapSymbols;
@property(nonatomic) NSDictionary * decMapIntLet;
@property(nonatomic) NSDictionary * decMapRusLet;
@property(nonatomic) NSDictionary * decMapSymbols;


-(void)encode;
-(void)decode;

@end
@implementation TextCodec
-(NSDictionary*)encMapIntLet
{
    if (!_encMapIntLet)
    {
        _encMapIntLet = [[NSDictionary alloc]initWithObjectsAndKeys:
                         @".-",     @"a",
                         @"-...",   @"b",
                         @"-.-.",   @"c",
                         @"-..",    @"d",
                         @".",      @"e",
                         @"..-.",   @"f",
                         @"--.",    @"g",
                         @"....",   @"h",
                         @"..",     @"i",
                         @".---",   @"j",
                         @"-.-",    @"k",
                         @".-..",   @"l",
                         @"--",     @"m",
                         @"-.",     @"n",
                         @"---",    @"o",
                         @".--.",   @"p",
                         @"--.-",   @"q",
                         @".-.",    @"r",
                         @"...",    @"s",
                         @"-",      @"t",
                         @"..-",    @"u",
                         @"...-",   @"v",
                         @".--",    @"w",
                         @"-..-",   @"x",
                         @"-.--",   @"y",
                         @"--..",   @"z",
                         @".-.-.-", @".",
                         @"--..--", @",",
                         nil];
    }
    return _encMapIntLet;
}

-(NSDictionary*)encMapRusLet
{
    if (!_encMapRusLet)
    {
        _encMapRusLet = @{
                          @"а" : @".-",
                          @"б" : @"-...",
                          @"в" : @".--",
                          @"г" : @"--.",
                          @"д" : @"-.." ,
                          @"е" : @".",
                          @"ё" : @".",
                          @"ж" : @"...-",
                          @"з" : @"--..",
                          @"и" : @"..",
                          @"й" : @".---",
                          @"к" : @"-.-",
                          @"л" : @".-..",
                          @"м" : @"--",
                          @"н" : @"-.",
                          @"о" : @"---",
                          @"п" : @".--.",
                          @"р" : @".-.",
                          @"с" : @"..." ,
                          @"т" : @"-",
                          @"у" : @"..-",
                          @"ф" : @"..-.",
                          @"х" : @"....",
                          @"ц" : @"-.-.",
                          @"ч" : @"---.",
                          @"ш" : @"----",
                          @"щ" : @"--.-",
                          @"ъ" : @"--.--",
                          @"ы" : @"-.--",
                          @"ь" : @"-..-",
                          @"э" : @"..-..",
                          @"ю" : @"..--",
                          @"я" : @".-.-",
                          @"." : @"......",
                          @"," : @".-.-.-"
                          };
    }
    return _encMapRusLet;
}
-(NSDictionary*)encMapSymbols
{
    if (!_encMapSymbols)
    {
        _encMapSymbols = @{
                           @"1": @".----",
                           @"2": @"..---",
                           @"3": @"...--",
                           @"4": @"....-",
                           @"5": @".....",
                           @"6": @"-....",
                           @"7": @"--...",
                           @"8": @"---..",
                           @"9": @"----.",
                           @"0": @"-----",
                           @":": @"---...",
                           @";": @"-.-.-.",
                           @"(": @"-.--.",
                           @")": @"-.--.-",
                           @"'": @".----.",
                           @"\"": @".-..-.",
                           @"-": @"-....-",
                           @"/": @"-..-.",
                           @"?": @"..--..",
                           @"!": @"--..--",
                           @"\n": @"-...-",
                           @"^": @"........",
                           @"@": @".--.-.",
                           @"~": @"..-.-",
                           @" ": @"^",
                           };
    }
    return _encMapSymbols;
}

-(NSDictionary*)decMapIntLet
{
    if(!_decMapIntLet)
    {
        NSMutableDictionary * tempDict = [[NSMutableDictionary alloc]init];

        for (NSString *key in [self.encMapIntLet allKeys]) {
            if (![key isEqualToString:@"ё"]) {
                tempDict[self.encMapIntLet[key]] = key;
            }
        }
        _decMapIntLet = tempDict;
        tempDict = nil;
    }
    return _decMapIntLet;
}
-(NSDictionary*)decMapRusLet
{
    if(!_decMapRusLet)
    {
        NSMutableDictionary * tempDict = [[NSMutableDictionary alloc]init];

        //swap key and value
        for (NSString *key in [self.encMapRusLet allKeys]) {

            if (![key isEqualToString:@"ё"]) {
                tempDict[self.encMapRusLet[key]] = key;
            }
        }
        _decMapRusLet = tempDict;
        tempDict = nil;

    }
    return _decMapRusLet;
}

-(NSDictionary*)decMapSymbols
{
    if(!_decMapSymbols)
    {
        NSMutableDictionary * tempDict = [[NSMutableDictionary alloc]initWithDictionary:self.encMapSymbols];
        //swap key and value
        for (NSString *key in [tempDict allKeys]) {
            tempDict[tempDict[key]] = key;
            [tempDict removeObjectForKey:key];
        }
        _decMapSymbols = [[NSDictionary alloc]initWithDictionary:tempDict];
        tempDict = nil;
        
    }
    return _decMapSymbols;
}

- (void)setText:(NSString *)text
{
    self.decodedText = text;
    [self encode];
}

- (void)setMorse:(NSString *)morseCode
{
    NSMutableString * result = [[NSMutableString alloc]init];
    for (int i = 0; i < [morseCode length]; i++) {
        unichar currentChar = [morseCode characterAtIndex:i];
        if (currentChar == '-' || currentChar == '.' || currentChar == ' ') {
            [result appendString:[NSString stringWithCharacters:&currentChar length:1]];
        }
    }
    self.encodedText = result;
    [self decode];
}

-(void)	encode
{
    int pos = 0;
    NSMutableString * result = [[NSMutableString alloc]init];
    
    while(pos < [self.decodedText length])
    {
        unichar currChar = [self.decodedText characterAtIndex:pos];
        NSString * currCharString = [[NSString stringWithCharacters:&currChar length:1] lowercaseString];
        
        if(self.locale == INT)
        {
            if([self.encMapIntLet  objectForKey:currCharString])	//if current character is latin, then encode it with map;
                
            {
                [result appendString: [self.encMapIntLet  objectForKey:currCharString]];
            }
            else
            {
                if([self.encMapSymbols objectForKey:currCharString])
                {
                    [result appendString:[self.encMapSymbols objectForKey:currCharString]];
                }
            }
            
        }
        
        if(self.locale == RUS)
        {
            if([self.encMapRusLet objectForKey:currCharString])
            {
                [result appendString:[self.encMapRusLet objectForKey:currCharString]];
            }
            else
            {
                if([self.encMapSymbols objectForKey:currCharString])
                {
                    [result appendString:[self.encMapSymbols objectForKey:currCharString]];
                }
            }
            
        }
        if(self.locale == ALL)
        {
            //cause period and comma has different Morse code for each locale, it need extra check
            if([currCharString isEqualToString:@"."] || [currCharString isEqualToString:@","]){
                if(self.localeByDefault == RUS){
                    [result appendString:[self.encMapRusLet objectForKey:currCharString]];
                }else{
                    [result appendString:[self.encMapIntLet objectForKey:currCharString]];
                 }
            }else{
				
                if([self.encMapIntLet objectForKey:currCharString])
                {
                    [result appendString:[self.encMapIntLet objectForKey:currCharString]];
                }
                else{
                    if([self.encMapRusLet objectForKey:currCharString])
                    {
                        [result appendString:[self.encMapRusLet objectForKey:currCharString]];
                    }else
                        if([self.encMapSymbols objectForKey:currCharString])
                        {
                            [result appendString:[self.encMapSymbols objectForKey:currCharString]];
                        }
                }
            }
        }
        pos++;
        [result appendString:@" "];
    }
    self.encodedText = [[result stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
}

-(void) decode
{
    NSMutableString * result = [[NSMutableString alloc]init];
    
    NSArray * characters = [self.encodedText componentsSeparatedByString:@" "];
    
    
    if(self.locale == RUS){
        for (NSString*  character in characters) {
            if([self.decMapRusLet objectForKey: character]){
                [result appendString:[self.decMapRusLet objectForKey:character]];
            }else if([self.decMapSymbols objectForKey: character]){
                [result appendString:[self.decMapSymbols objectForKey:character]];
            }else{
                [result appendString:@"?"];
            }
        }
    }else{
        for (NSString*  character in characters) {
            if([self.decMapIntLet objectForKey: character]){
                [result appendString:[self.decMapIntLet objectForKey:character]];
            }else if([self.decMapSymbols objectForKey: character]){
                [result appendString:[self.decMapSymbols objectForKey:character]];
            }else{
                [result appendString:@"?"];
            }
        }
        
    }
    self.decodedText = [result uppercaseString];
}

-(instancetype)init
{
    if (self = [super init])
    {
        Settings * settings = [Settings sharedSettings];
        self.localeByDefault = settings.locale;
        self.locale = self.localeByDefault;
        settings = nil;
    }
    return self;
}
@end
