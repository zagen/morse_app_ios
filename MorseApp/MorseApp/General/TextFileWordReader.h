//
//  TextFileWordReader.h
//  MorseApp
//
//  Created by Admin on 04.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TextFileWordReader : NSObject
@property (strong, nonatomic) NSString * filename;
@property (nonatomic) float percent;

-(BOOL)hasFile;
-(NSString *)nextWord;
-(NSString *)previousWord;
-(void) seekToNextWord;
-(void) seekToPreviousWord;
-(void) seekToNextTenWord;
-(void) seekToPreviousTenWord;
-(void) gotoPercent:(float)percent;
-(void)saveProgress;

@end
