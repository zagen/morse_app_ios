//
//  NSString+CompareWithAttributeStringResult.m
//  MorseApp
//
//  Created by Admin on 07.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "NSString+CompareWithAttributeStringResult.h"

@implementation NSString (CompareWithAttributeStringResult)
-(NSAttributedString *)highlightDifferentFromString:(NSString *)correctString correctColor:(UIColor *)correctColor wrongColor:(UIColor *)wrongColor andSize:(float)textSize
{
    NSMutableAttributedString * line = [[NSMutableAttributedString alloc]initWithString:correctString attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:textSize]}];
    
    NSString * inputedString = [self stringByReplacingOccurrencesOfString:@"Ё" withString:@"Е"];
    correctString = [correctString stringByReplacingOccurrencesOfString:@"Ё" withString:@"Е"];
    
    NSLog(@"Inputed string : %@", inputedString);
    
    NSLog(@"Correct string : %@", correctString);
    if( ![correctString isEqualToString:inputedString])
    {
        int minLength = ([correctString length] > [inputedString length])? [inputedString length]: [correctString length];
        for (int i = 0; i < minLength; i++) {
            if ([correctString characterAtIndex:i] != [inputedString characterAtIndex:i]) {
                [line addAttribute:NSForegroundColorAttributeName value:wrongColor range:NSMakeRange(i, 1)];
            }else{
                [line addAttribute:NSForegroundColorAttributeName value:correctColor range:NSMakeRange(i, 1)];
            }
            
        }
        if ([correctString length] > minLength) {
            [line addAttribute:NSForegroundColorAttributeName value:wrongColor range:NSMakeRange(minLength, [line length] - minLength)];
            
        }
    }else{
        
        [line addAttribute:NSForegroundColorAttributeName value:correctColor range:NSMakeRange(0, [line length])];
    }
    
    return line;
}

@end
