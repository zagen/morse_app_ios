//
//  UILabel+ShowPopup.h
//  MorseApp
//
//  Created by Admin on 18.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (ShowPopup)
-(void)showPopup:(NSString *)message;
@end
