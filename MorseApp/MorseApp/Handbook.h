//
//  Handbook.h
//  MorseApp
//
//  Created by Admin on 22.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Handbook : NSObject
-(int)charactersCount;
-(NSString *)characterAtIndex:(int)index;
-(NSString *)characterNameAtIndex:(int)index;
-(NSString *)mnemonicAtIndex:(int)index;
-(NSString *)morseCodeOfCharacterWithIndex:(int)index;
@end
