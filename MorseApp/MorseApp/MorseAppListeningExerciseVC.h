//
//  MorseAppListeningExerciseVCViewController.h
//  MorseApp
//
//  Created by Admin on 28.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "General/TransmitterDelegate.h"

@interface MorseAppListeningExerciseVC : UIViewController<TransmitterDelegate, UITextFieldDelegate>

@end
