//
//  TouchPadViewDelegate.h
//  MorseApp
//
//  Created by Admin on 06.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TouchPadViewDelegate <NSObject>
-(void) inputed:(NSString *)symbol;
@end
