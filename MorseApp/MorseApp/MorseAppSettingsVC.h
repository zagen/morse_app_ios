//
//  MorseAppSettingsVC.h
//  MorseApp
//
//  Created by Admin on 20.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SliderCellDelegate.h"
#import "SwitcherCellDelegate.h"
#import "Settings.h"

@interface MorseAppSettingsVC : UIViewController<UITableViewDataSource, UITableViewDelegate, SliderCellDelegate, SwitcherCellDelegate>

+(NSArray *)transmitters;
+(NSArray *)locals;

@end
