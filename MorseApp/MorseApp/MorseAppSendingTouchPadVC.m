//
//  MorseAppSendingTouchPadVC.m
//  MorseApp
//
//  Created by Admin on 05.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "MorseAppSendingTouchPadVC.h"
#import "MorseCodeStackView.h"
#import "TouchPadView.h"
#import "General/Generator.h"
#import "General/UILabel+ShowPopup.h"
#import "General/NSString+CompareWithAttributeStringResult.h"
#import "MorseAppResultVC.h"

@interface MorseAppSendingTouchPadVC ()
@property (weak, nonatomic) IBOutlet UILabel *correctAnswerInfoLabel;
@property (weak, nonatomic) IBOutlet MorseCodeStackView *morseView;
@property (weak, nonatomic) IBOutlet TouchPadView *touchPad;
@property (weak, nonatomic) IBOutlet UIButton *generalViewButton;

@property (weak, nonatomic) IBOutlet UIButton *reloadButton;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@property (strong, nonatomic) NSString * currentText;
@property (strong, nonatomic) Generator * generator;

@property (strong, nonatomic) NSMutableArray * resultData;
@property (strong, nonatomic) NSMutableString * inputedMorseCode;
@property (strong, nonatomic) TextCodec * codec;
@property (nonatomic) int inputedCharsCount;
@end

@implementation MorseAppSendingTouchPadVC



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.okButton.layer.cornerRadius = self.okButton.frame.size.width/2;
    self.reloadButton.layer.cornerRadius = self.reloadButton.frame.size.width/2;
    self.generalViewButton.layer.cornerRadius = 15;
    [self reload:nil];
    self.morseView.symbolSize = 7;
    self.touchPad.delegate = self;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(Generator *)generator
{
    if (!_generator) {
        _generator = [[Generator alloc]init];
    }
    return _generator;
}
-(NSMutableArray *)resultData
{
    if (!_resultData) {
        _resultData = [[NSMutableArray alloc]init];
    }
    return _resultData;
}
-(NSMutableString *)inputedMorseCode
{
    if (!_inputedMorseCode) {
        _inputedMorseCode = [[NSMutableString alloc]init];
    }
    return _inputedMorseCode;
}
-(TextCodec *)codec
{
    if (!_codec) {
        _codec = [[TextCodec alloc]init];
    }
    return _codec;
}
-(void) resetUI
{
    if (self.currentText) {
        int sequenceLength = (int)[self.currentText length];
        
        NSMutableAttributedString *title = [[NSMutableAttributedString alloc]initWithString:self.currentText];
        NSRange range = NSMakeRange(0, sequenceLength);
        
        [title addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
        [self.generalViewButton setAttributedTitle:title forState:UIControlStateNormal];
        
        [self.generalViewButton setTitle:self.currentText forState:UIControlStateNormal];
        self.touchPad.enabled = YES;
        self.inputedCharsCount = 0;
        [self.inputedMorseCode setString:@""];
        [self.morseView clear];
        self.correctAnswerInfoLabel.hidden = YES;
    }
}
- (void) reload
{
    self.currentText = [self.generator next];
    [self resetUI];
}

- (IBAction)reload:(id)sender {
    [self reload];
    [self.statusLabel showPopup:@"New sequence generated"];
}
- (IBAction)ok:(id)sender {
    if ([self.resultData count]) {
        [self performSegueWithIdentifier:@"touchPadSendingExerciseToResultSegue" sender:self];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (IBAction)tryAgain:(id)sender {
    [self resetUI];
    [self.statusLabel showPopup:@"Try again"];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"touchPadSendingExerciseToResultSegue"]) {
        MorseAppResultVC * resultVC = [segue destinationViewController];
        resultVC.resultsData = [self.resultData copy];
    }
}

-(void)inputed:(NSString *)symbol
{

    if ([symbol isEqualToString:@"   "]) {
        return;
    }
    [self.morseView addMorseCode:symbol];
    [self.inputedMorseCode appendString:symbol];
    //NSLog(@"Inputed char \"%@\"", symbol);
    if ([symbol isEqualToString: @" "]) {
        self.inputedCharsCount++;
        [self checkInput];
    }
}
-(void)checkInput
{
    if ([self.currentText length] == self.inputedCharsCount) {
        NSLog(@"Check");
        self.touchPad.enabled = NO;
        
        [self.codec setMorse: [self.inputedMorseCode stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        NSLog(@"Inputed morse code: \"%@\"", self.codec.encodedText);
        NSString *inputedText = self.codec.decodedText;
        NSLog(@"Inputed text: \"%@\"", inputedText);
        
        [self.resultData addObject: @{self.currentText : inputedText}];
        
        if ([self.currentText isEqualToString: inputedText]) {
            [self reload];
            [self.statusLabel showPopup:@"Answer is correct! New sequence generated!"];
            
        }else{
            NSAttributedString *title = [inputedText highlightDifferentFromString:self.currentText
                                                                     correctColor:[UIColor blackColor]
                                                                       wrongColor:[UIColor lightGrayColor]
                                                                          andSize: 27];
            
            [self.generalViewButton setAttributedTitle:title forState:UIControlStateNormal];
            [self.morseView clear];
            [self.codec setText:self.currentText];
            [self.morseView addMorseCode:self.codec.encodedText];
            self.correctAnswerInfoLabel.hidden = NO;
            [self.statusLabel showPopup:@"Incorrect answer!"];
        }
    }
}

@end
