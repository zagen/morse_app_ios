//
//  MorseAppFreepadVC.h
//  MorseApp
//
//  Created by Admin on 12.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TouchPadViewDelegate.h"

@interface MorseAppFreepadVC : UIViewController<UIAlertViewDelegate, TouchPadViewDelegate>

@end
