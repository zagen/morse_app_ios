//
//  main.m
//  MorseApp
//
//  Created by Admin on 18.05.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MorseAppAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MorseAppAppDelegate class]));
    }
}
