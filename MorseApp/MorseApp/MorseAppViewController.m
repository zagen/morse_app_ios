//
//  MorseAppViewController.m
//  MorseApp
//
//  Created by Admin on 18.05.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "MorseAppViewController.h"
#import "MorseAppResultVC.h"
#import "MorseCodeStackView.h"
#import "General/Generator.h"
#import "General/TextCodec.h"
#import "General/UILabel+ShowPopup.h"

@interface MorseAppViewController ()
@property (weak, nonatomic) IBOutlet UIButton *generalViewButton;
@property (weak, nonatomic) IBOutlet UIButton *reloadButton;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UILabel *answerStatus;
@property (weak, nonatomic) IBOutlet MorseCodeStackView *morseStackView;
@property (weak, nonatomic) IBOutlet UILabel *informationLabel;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *inputButtons;

@property (strong, nonatomic) TextCodec *codec;
@property (strong, nonatomic) Generator *generator;
@property (strong, nonatomic) NSMutableString *inputed;
@property (strong, nonatomic) NSMutableArray * result; //of Dictionary @{@"key": @"value"}
@end

@implementation MorseAppViewController

-(TextCodec *)codec
{
    if(!_codec)
    {
        _codec = [[TextCodec alloc]init];

    }
    return _codec;
}

-(NSMutableArray *)result
{
    if(!_result)
    {
        _result = [[NSMutableArray alloc]init];
    }
    return _result;
}

-(NSMutableString *)inputed
{
    if(!_inputed){
        _inputed = [[NSMutableString alloc]init];
    }
    return _inputed;
}
-(Generator *) generator
{
    if(!_generator)
    {
        _generator = [[Generator alloc]init];
    }
    return _generator;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.generalViewButton.layer.cornerRadius = 15;

    self.reloadButton.layer.cornerRadius = self.reloadButton.frame.size.width/2;
    self.okButton.layer.cornerRadius = self.okButton.frame.size.width/2;

    self.morseStackView.symbolSize = 7;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self reload:nil];
}
- (IBAction)reload:(id)sender
{
    
    NSString * sequence = [self.generator next];
    int sequenceLength = (int)[sequence length];
    NSMutableAttributedString *title = [[NSMutableAttributedString alloc]initWithString:sequence];
    NSRange range = NSMakeRange(0, sequenceLength);
    
    [title addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
    [self.generalViewButton setAttributedTitle:title forState:UIControlStateNormal];

    [self.morseStackView clear];
    self.answerStatus.hidden = YES;
    
    for(UIButton * inputButton in self.inputButtons)
    {
        inputButton.enabled = YES;
    }
    [self.informationLabel showPopup:@"New sequence generated"];
}
- (IBAction)dotTouch:(id)sender
{
    [self.morseStackView addMorseCode:@"."];
    [self.inputed appendString:@"."];
}
- (IBAction)spaceTouch:(id)sender
{
    if ([self.inputed length] != 0 && [self.inputed characterAtIndex:[self.inputed length] - 1] != ' ') {
        [self.morseStackView addMorseCode:@" "];
        [self.inputed appendString:@" "];
    }
}
- (IBAction)dashTouch:(id)sender
{
     [self.morseStackView addMorseCode:@"-"];
     [self.inputed appendString:@"-"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)enterTouch:(id)sender {
    //check if inputed and right answers equal;

    [self.codec setMorse:self.inputed];
    
    self.inputed = [[NSMutableString alloc]init];
    NSLog(@"Decoded text: %@", self.codec.decodedText);
    
    NSString * decodedInputed = self.codec.decodedText;
    NSString * decodedCorrect = [self.generalViewButton.currentAttributedTitle string];

    [self.codec setText:decodedCorrect];
    NSString * encodedCorrect = self.codec.encodedText;
    
    
    if ([decodedCorrect isEqualToString:decodedInputed]) {
        self.answerStatus.hidden = YES;
        //put message : answer correct and reload;
        [self reload:nil];
        [self.informationLabel showPopup:@"Answer correct"];
        
    }else{
        self.answerStatus.hidden = NO;
        //output correct answer and highlight incorrect chars;
        [self.morseStackView clear];
        [self.morseStackView addMorseCode:encodedCorrect];
        
        for(UIButton * inputButton in self.inputButtons)
        {
            inputButton.enabled = NO;
        }
        //highlightning of wrong inputed symbols
        int inputedLength = (int)[decodedInputed length];
        int correctLength = (int)[decodedCorrect length];
        int maxCheckLength = correctLength;
        NSMutableAttributedString * highlightedButtonTitle = [self.generalViewButton.currentAttributedTitle mutableCopy];
        highlightedButtonTitle = (!highlightedButtonTitle)? [[NSMutableAttributedString alloc]initWithString:decodedCorrect]: highlightedButtonTitle;
        
        if(correctLength > inputedLength)
        {
            //show MESSAGE : INPUTED TEXT LENGTH MORE THEN CORRECT ONE
            maxCheckLength = inputedLength;
            //highlight symbols than outgrow inputed
            NSRange range = NSMakeRange(inputedLength,
                                        correctLength - inputedLength);
            [highlightedButtonTitle addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:range];
            
        } else if (correctLength < inputedLength) {
                //show MESSAGE : INPUTED LENGTH LESS THEN CORRECT ONE
        }
        

        for (int i = 0; i < maxCheckLength; i++) {
            if([decodedCorrect characterAtIndex:i] != [decodedInputed characterAtIndex:i])
            {
                NSRange range = NSMakeRange(i, 1);
                [highlightedButtonTitle addAttribute:NSForegroundColorAttributeName value:[UIColor lightGrayColor] range:range];
            }
        }
       
                [self.generalViewButton setAttributedTitle:highlightedButtonTitle forState:UIControlStateNormal];
    }
    [self.result addObject:@{decodedCorrect : decodedInputed}];

}
- (IBAction)clearInput:(id)sender
{
    //button attribute clear
    
    [self.morseStackView clear];
    self.inputed = [[NSMutableString alloc]init];
    self.answerStatus.hidden = YES;
    
    for(UIButton * inputButton in self.inputButtons)
    {
        inputButton.enabled = YES;
    }
    
    NSMutableAttributedString * highlightedButtonTitle = [self.generalViewButton.currentAttributedTitle mutableCopy];

    int sequenceLength = (int)[self.generalViewButton.currentAttributedTitle length];
    NSRange range = NSMakeRange(0, sequenceLength);
    [highlightedButtonTitle addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];

    [self.generalViewButton setAttributedTitle:highlightedButtonTitle forState:UIControlStateNormal];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"sendingKeyboardToResultSegue"])
    {
        // Get reference to the destination view controller
        MorseAppResultVC *resultVC = [segue destinationViewController];
        
        resultVC.resultsData = [self.result copy];
        
        self.result = nil;
        
        NSLog(@"%lu in prepare", (unsigned long)[self.result count]);
    }
}
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"sendingKeyboardToResultSegue"]) {
        if([self.result count]){
            return YES;
        }else{
            [self.navigationController popViewControllerAnimated:YES];
            return NO;
        }

    
    }else
        return YES;
}
@end
