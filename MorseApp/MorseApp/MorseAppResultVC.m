//
//  MorseAppResultVC.m
//  MorseApp
//
//  Created by Admin on 19.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "MorseAppResultVC.h"
#import "Statistics.h"

@interface MorseAppResultVC ()


@property (weak, nonatomic) IBOutlet UIButton *okButton;

@property (weak, nonatomic) IBOutlet UITableView *table;

@property (weak, nonatomic) IBOutlet UILabel *statisticLabel;

@property (strong, nonatomic) Statistics * statistics;
@end

@implementation MorseAppResultVC

- (IBAction)back:(id)sender {
    if (self.returnToPreviousViewController) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    self.resultsData = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.okButton.layer.cornerRadius = self.okButton.frame.size.width/2;
    [self.table setDataSource:self];
    [self.table setDelegate:self];
    self.statisticLabel.text = [NSString stringWithFormat:@"Total score:\n%d - %d \n%.2f%%- 100%%",self.statistics.countOfCorrectInputedCharacters,self.statistics.totalCountOfCharacters, [self.statistics percentageOfCorrectness]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSLog(@"%lu size", (unsigned long)[self.resultsData count]);
    return [self.resultsData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"resultCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    NSString *correctValue = @"";
    NSString *inputedValue = @"";
    NSDictionary *pair = self.resultsData[indexPath.row];
    NSArray *valuesArray = [pair allKeys];
    if ([valuesArray count])
    {
        correctValue = valuesArray[0];
        inputedValue = pair[correctValue];
    }

    cell.textLabel.text = correctValue;
    cell.detailTextLabel.text= inputedValue;
    
    return cell;
}

-(Statistics *)statistics
{
    if(!_statistics)
    {
        _statistics = [[Statistics alloc]initWithResultData:self.resultsData];
    }
    return _statistics;
}
@end
