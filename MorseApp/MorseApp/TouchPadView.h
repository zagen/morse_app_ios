//
//  TouchPadView.h
//  MorseApp
//
//  Created by Admin on 05.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioUnit/AudioUnit.h>
#import "TouchPadViewDelegate.h"

@interface TouchPadView : UIView{
    AudioComponentInstance toneUnit;
@public
    double frequency;
    double sampleRate;
    double theta;

}

@property (nonatomic) BOOL isSignal;
@property (nonatomic) int position;
@property (nonatomic) int fadeLength;
@property (nonatomic) BOOL isFadeIn;
@property (nonatomic) BOOL isFadeOut;
@property (nonatomic, getter = isEnabled) BOOL enabled;
@property (strong, nonatomic) id<TouchPadViewDelegate> delegate;
@end
