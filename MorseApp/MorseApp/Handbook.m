//
//  Handbook.m
//  MorseApp
//
//  Created by Admin on 22.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "Handbook.h"
#import "TextCodec.h"
#import "Settings.h"

@interface Handbook()
@property (strong, nonatomic) Settings * settings;
@property (strong, nonatomic) TextCodec * codec;
@property (strong, nonatomic) NSArray * characters;// of NSString
@property (strong, nonatomic) NSDictionary * mnemonics;
@property (strong, nonatomic) NSDictionary * names;

@end
@implementation Handbook

-(TextCodec *)codec
{
    if (!_codec)
    {
        _codec = [[TextCodec alloc]init];
        _codec.locale =  (self.settings.locale == 1)? RUS : INT;
    }
    return _codec;
}
-(Settings *)settings
{
    if (!_settings) {
        _settings = [Settings sharedSettings];
    }
    return _settings;
}

-(NSArray *)characters
{
    if (!_characters){
        NSMutableArray * chars = [[NSMutableArray alloc]init];
        NSString *abc = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890.,:;()\"'-/?!@";
        if (self.settings.locale == 1) {
            abc = @"АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ1234567890.,:;()\"'-/?!@";
        }
        for (int i = 0; i < (int)[abc length]; i++) {
            unichar currentChar = [abc characterAtIndex:i];
            [chars addObject:[NSString stringWithCharacters:&currentChar
                                                     length:1]];
        }
        _characters = chars;
    }
    return _characters;
}
-(NSDictionary *)mnemonics
{
    if (!_mnemonics) {
        if (self.settings.locale == 1) {
            _mnemonics = @{
                           @"А": @"ай-даа",
                           @"Б": @"баа-ки-те-кут",
                           @"В": @"ви-даа-лаа",
                           @"Г": @"гаа-раа-ж",
                           @"Д": @"доо-ми-ки",
                           @"Е": @"есть",
                           @"Ё": @"есть",
                           @"Ж": @"я-бук-ва-жее",
                           @"З": @"заа-каа-ти-ки",
                           @"И": @"и-ди",
                           @"Й": @"и-краат-коо-ее",
                           @"К": @"каак-де-лаа",
                           @"Л": @"лу-наа-ти-ки",
                           @"М": @"маа-маа",
                           @"Н": @"ноо-мер",
                           @"О": @"оо-коо-лоо",
                           @"П": @"пи-лаа-поо-ёт",
                           @"Р": @"ре-шаа-ет",
                           @"С": @"са-мо-лёт",
                           @"Т": @"таак",
                           @"У": @"у-нес-лоо",
                           @"Ф": @"фи-ли-моон-чик",
                           @"Х": @"хи-ми-чи-те",
                           @"Ц": @"цаа-пли-наа-ши",
                           @"Ч": @"чее-лоо-вее-чек",
                           @"Ш": @"шаа-роо-ваа-рыы",
                           @"Щ": @"щуу-каа-жи-ваа",
                           @"Ъ": @"ээ-тоо-твёр-дыый-знаак",
                           @"Ы": @"ыы-не-наа-доо",
                           @"Ь": @"тоо-мяг-кий-знаак",
                           @"Э": @"э-ле-ктроо-ни-ки",
                           @"Ю": @"ю-ли-аа-наа",
                           @"Я": @"я-маал-я-маал",
                           
                           @"1": @"и-тооль-коо-оо-днаа",
                           @"2": @"две-не-хоо-роо-шоо",
                           @"3": @"три-те-бе-маа-лоо",
                           @"4": @"че-тве-ри-те-каа",
                           @"5": @"пя-ти-ле-ти-е",
                           @"6": @"поо-шес-ти-бе-ри",
                           @"7": @"даай-даай-за-ку-рить",
                           @"8": @"воо-сьмоо-гоо-и-ди",
                           @"9": @"ноо-наа-ноо-наа-ми",
                           @"0": @"нооль-тоо-оо-коо-лоо",
                           @".": @"то-чеч-ка-то-чеч-ка",
                           @",": @"крю-чоок-крю-чоок-крю-чоок",
                           @":": @"двоо-ее-тоо-чи-е-ставь",
                           @";": @"тоо-чка-заа-пя-таа-я",
                           @"(": @"скоо-бку-стаавь-скоо-бку-стаавь",
                           @")": @"скоо-бку-стаавь-скоо-бку-стаавь",
                           @"'": @"крю-чоок-тыы-веерх-ниий-ставь",
                           @"\"": @"ка-выы-чки-ка-выы-чки",
                           @"-": @"чёёр-точ-ку-мне-да-ваай",
                           @"/": @"дрообь-здесь-пред-стаавь-те,",
                           @"?": @"вы-ку-даа-смоо-три-те",
                           @"!": @"оо-наа-вос-кли-цаа-лаа",
                           @"@": @"со-баа-каа-ку-саа-ет"
                           };
        }else{
            _mnemonics = @{
                           @"A": @"al-FA",
                           @"B": @"YEAH! *clap* *clap* *clap*",
                           @"C": @"CHAR-lie's AN-gels",
                           @"D": @"NEW or-leans",
                           @"E": @"hey!",
                           @"F": @"*step**step**BRUSH**step*",
                           @"G": @"HOLE IN one!",
                           @"H": @"ho-li-day-inn",
                           @"I": @"bom-bay",
                           @"J": @"where-FORE ART THOU?",
                           @"K": @"POUND for POUND",
                           @"L": @"li-MA pe-ru",
                           @"M": @"LIKE MIKE",
                           @"N": @"AU-tumn",
                           @"O": @"SUN-NY-DAY",
                           @"P": @"of DOC-TOR good",
                           @"Q": @"A-LOU-et-TUH",
                           @"R": @"ro-MER-o",
                           @"S": @"ne-va-da",
                           @"T": @"*DIP*",
                           @"U": @"u-ni-FORM",
                           @"V": @"the first 4 notes of Beethoven's 5th",
                           @"W": @"jack AND COKE",
                           @"X": @"ON-ly the BONES",
                           @"Y": @"ON a PO-NY",
                           @"Z": @"SHA-KA zu-lu",
                           
                           @"1": @"i-WON-WON-WON-WON",
                           @"2": @"cut-it-SAW-SAW-SAW",
                           @"3": @"make-it-a-THREE-SOME",
                           @"4": @"did-he-hol-ler-FOUR?",
                           @"5": @"sit-a-hal-i-but",
                           @"6": @"SIX-hav-ing-a-fit",
                           @"7": @"SEV-EN-bet-on-it",
                           @"8": @"MOM-IN-LAW-ate-it",
                           @"9": @"NINE-NINE-NINE-NINE-zip",
                           @"0": @"NUL-NUL-NUL-NUL-NUL",
                           @".": @"a STOP a STOP a STOP",
                           @",": @"COM-MA, it's a COM-MA",
                           @":": @"HA-WA-II stan-dard time",
                           @";": @"A-list, B-list, C-list",
                           @"(": @"AU-tumn HOLE-IN-one!",
                           @")": @"AU-tumn A-LOU-et-TUH",
                           @"'": @"and THIS STUFF GOES TO me!",
                           @"\"": @"six-TY-six nine-TY-nine",
                           @"-": @"*DIP* ho-li-day-inn *DIP*",
                           @"/": @"SHAVE and a HAIR-cut",
                           @"?": @"it's a QUES-TION, is it?",
                           @"!": @"AU-tumn ON a PO-NY",
                           @"@": @"al-FA CHAR-lie's AN-gels"
                           
                           };
        }
        
    }
    return _mnemonics;
}
-(NSDictionary *)names{
    if (!_names) {
        _names = @{
                   @"A": @"Alpha",
                   @"B": @"Bravo",
                   @"C": @"Charlie",
                   @"D": @"Delta",
                   @"E": @"Echo",
                   @"F": @"Foxtrot",
                   @"G": @"Golf",
                   @"H": @"Hotel",
                   @"I": @"India",
                   @"J": @"Juliet",
                   @"K": @"Kilo",
                   @"L": @"Lima",
                   @"M": @"Mike",
                   @"N": @"November",
                   @"O": @"Oscar",
                   @"P": @"Papa",
                   @"Q": @"Quebec",
                   @"R": @"Romeo",
                   @"S": @"Sierra",
                   @"T": @"Tango",
                   @"U": @"Uniform",
                   @"V": @"Victor",
                   @"W": @"Whiskey",
                   @"X": @"X-ray",
                   @"Y": @"Yankee",
                   @"Z": @"Zulu"
                   };
    }
    return  _names;
}
-(int)charactersCount
{
    return (int)[self.characters count];
}
-(NSString *)characterAtIndex:(int)index
{
    if([self.characters objectAtIndex:index])
    {
        return [self.characters objectAtIndex:index];
    }else
        return @"";
}
-(NSString *)characterNameAtIndex:(int)index
{
    NSString * character = [self.characters objectAtIndex:index];
    if(character){
        if ([self.names objectForKey:character]) {
            return [self.names objectForKey:character];
        }
    }
    return @"";
}

-(NSString *)mnemonicAtIndex:(int)index
{
    NSString * character = [self.characters objectAtIndex:index];
    if(character){
        if ([self.mnemonics objectForKey:character]) {
            return self.mnemonics[character];
        }
    }
    return @"";
}
-(NSString *)morseCodeOfCharacterWithIndex:(int)index
{
    NSString * character = [self.characters objectAtIndex:index];
    if(character){
        [self.codec setText:character];
        return self.codec.encodedText;
    }
    return @"";
}
@end
