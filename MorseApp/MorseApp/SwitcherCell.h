//
//  SwitcherCell.h
//  MorseApp
//
//  Created by Admin on 21.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwitcherCellDelegate.h"

@interface SwitcherCell : UITableViewCell
@property (strong, nonatomic) id<SwitcherCellDelegate> delegate;

-(void) initWithLabelText:(NSString *)text andSwitchValue:(BOOL)value;

@end
