//
//  NoMistakeAchievement.m
//  MorseApp
//
//  Created by Admin on 25.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "NoMistakeAchievement.h"

@implementation NoMistakeAchievement
-(NSString *)name
{
    return @"Error-free coder";
}

-(int)showPriority
{
    return 50;
}

-(NSString*)description
{
    return @"Trophy for error-free coding more than 10 characters";
}

-(BOOL)fitToConditios:(NSArray *)resultData
{
    [self countCharacters:resultData];
    if(self.countOfCorrectInputedChars == self.countOfTotalInputedChars && self.countOfCorrectInputedChars >= 10)
    {
        return  YES;
    }
    return NO;
}

-(NSString *)imageName
{
    return @"ach_no_trophy";
}
@end
