//
//  MasterAchievement.m
//  MorseApp
//
//  Created by Admin on 25.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "MasterAchievement.h"

@implementation MasterAchievement

-(NSString *)name
{
    return @"Master";
}
-(int)showPriority
{
    return 70;
}
-(NSString *)description
{
    return @"Trophy for correct transmission more than 30 characters ";
}
-(BOOL)fitToConditios:(NSArray *)resultData
{
    [self countCharacters:resultData];
    if( self.countOfCorrectInputedChars >= 30)
    {
        return  YES;
    }
    return NO;
}
-(NSString *)imageName
{
    return @"ach_master";
}
@end
