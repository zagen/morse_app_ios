//
//  Achievement.m
//  MorseApp
//
//  Created by Admin on 22.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "Achievement.h"


@interface Achievement()
@property (nonatomic, readwrite) int countOfCorrectInputedChars;
@property (nonatomic, readwrite) int countOfTotalInputedChars;
@end
@implementation Achievement

-(BOOL)fitToConditios:(NSArray *)resultData //abstract method for analize data result
{
    return NO;
}
-(void)countCharacters:(NSArray *)resultData
{
    self.countOfCorrectInputedChars = 0;
    self.countOfTotalInputedChars = 0;
    for (int i = 0; i < [resultData count]; i++) {
        NSDictionary *oneResult = resultData[i];
        if([oneResult count])
        {
            NSString *correct = [[oneResult allKeys] objectAtIndex:0];
            NSString *inputed = [oneResult valueForKey:correct];
            int minLength = ([correct length] < [inputed length])? [correct length] : [inputed length];
            
            for (int j = 0; j < minLength; j++) {
                if( [correct characterAtIndex:j] == [inputed characterAtIndex:j])
                {
                    self.countOfCorrectInputedChars++;
                }
            }
            self.countOfTotalInputedChars += [correct length];
        }
    }
}
@end
