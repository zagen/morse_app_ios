//
//  AchievementsManager.h
//  MorseApp
//
//  Created by Admin on 26.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Settings.h"

@interface AchievementsManager : NSObject
+(NSArray *)allAchievementSubclasses;

@property (strong, nonatomic, readonly) NSArray *achieved;
@property (strong, nonatomic) Settings * settings;
-(NSArray *)analyze:(NSArray *)resultData; // take a result array of dictionary contains a pair correct - inputed value and return array of new achievements
-(BOOL)hadSomethingNewAchieved:(NSArray *)resultData;
+(id)sharedAchievementsManager;
@end
