//
//  AchievementsManager.m
//  MorseApp
//
//  Created by Admin on 26.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "AchievementsManager.h"
#import "Achievement.h"
#import <objc/runtime.h>
#import <objc/objc.h>

@interface AchievementsManager()
@property (strong,nonatomic) NSArray * allAchievements; //of Achievement
@property (strong, nonatomic, readwrite) NSArray *achieved;
@end

@implementation AchievementsManager

+(NSArray *)allAchievementSubclasses
{
    int numClasses = objc_getClassList(NULL, 0);
    Class *classes = NULL;
    
    if (numClasses > 0 )
    {
        classes = (Class *)malloc(sizeof(Class) * numClasses);
        numClasses = objc_getClassList(classes, numClasses);
        
    }
    NSMutableArray *result = [NSMutableArray array];
    if (classes) {
        
        for (NSInteger i = 0; i < numClasses; i++)
        {
            Class superClass = classes[i];
            do
            {
                superClass = class_getSuperclass(superClass);
            } while(superClass && superClass != [Achievement class]);
            
            if (superClass == nil)
            {
                continue;
            }
            
            [result addObject:classes[i]];
        }
        free(classes);
    }
    return result;
}

-(NSArray *)allAchievements
{
    if(!_allAchievements)
    {
        NSMutableArray * achievements = [[NSMutableArray alloc]init];
        for (Class class in [AchievementsManager allAchievementSubclasses])
        {
            [achievements addObject:[[class alloc]init]];
            
        }
        [achievements sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"showPriority" ascending:NO]]];
        _allAchievements = achievements;
    }
    return _allAchievements;
}
-(Settings *)settings
{
    if (!_settings) {
        _settings = [Settings sharedSettings];
    }
    return _settings;
}
-(NSArray *)achieved{
    if (!_achieved) {
        NSMutableArray *achievedAchievements = [[NSMutableArray alloc]init];
        
        for (NSString *achievementName in self.settings.achievements) {
            Achievement * achievement = [self achievementByName:achievementName];
            if (achievement) {
                [achievedAchievements addObject:achievement];
            }
        }
        _achieved = achievedAchievements;
        
    }
    return _achieved;
}
-(Achievement *)achievementByName:(NSString *)name
{
    for (Achievement * achievement in self.allAchievements) {
        if ([achievement.name isEqualToString:name]) {
            return achievement;
        }
    }
    return nil;
}

-(BOOL)hadSomethingNewAchieved:(NSArray *)resultData
{
    for (Achievement * achievement in self.allAchievements) {
        if ([achievement fitToConditios:resultData] && ![self achievementAlreadyAchieved:achievement]) {
            return YES;
        }
    }
    return NO;
}
-(NSArray *)analyze:(NSArray *)resultData
{
    NSMutableArray * newAchievements = [[NSMutableArray alloc]init];
    for (Achievement * achievement in self.allAchievements) {
        if ([achievement fitToConditios:resultData] && ![self achievementAlreadyAchieved:achievement]) {
            [newAchievements addObject:achievement];
            if ([achievement.name isEqualToString:@"Personal record"]) {
                [self removePersonalBestAchievementFromAchieved];
                self.settings.personalRecord = achievement.countOfCorrectInputedChars;
                [self.settings save];
                
            }
        }
    }
    if ([newAchievements count]) {
        self.achieved = [self.achieved arrayByAddingObjectsFromArray:newAchievements];
        NSMutableArray * settingsAchievements = [[NSMutableArray alloc]init];
        for (Achievement * achievement in self.achieved) {
            [settingsAchievements addObject:achievement.name];
        }
        self.settings.achievements = settingsAchievements;
        [self.settings save];
    }

    return newAchievements;
}


-(BOOL)achievementAlreadyAchieved:(Achievement*)achievement
{
    if ([achievement.name isEqualToString:@"Personal record"]) {
        return NO;
    }
    for (Achievement *achievedAchievement in self.achieved) {
       if([achievement.name isEqualToString:achievedAchievement.name]) {
           return YES;
        }
    }
    return NO;
}
-(void)removePersonalBestAchievementFromAchieved
{
    int indexOfRecordAchievement = -1;
    for (int i =  0; i < [self.achieved count]; i++) {
        Achievement * achievedAchievement = self.achieved[i];
        if ([achievedAchievement.name isEqualToString:@"Personal record"]) {
            indexOfRecordAchievement = i;
            break;
        }
        
    }
    if (indexOfRecordAchievement != -1){
        NSMutableArray *mutableAchieved = [self.achieved mutableCopy];
        [mutableAchieved removeObjectAtIndex:indexOfRecordAchievement];
        self.achieved = mutableAchieved;
    }
}
-(void)settingsChangedNotificationRecieved:(NSNotification*)notification
{
    if ([notification.name isEqualToString:@"SettingsChangedNotification"]) {
        [self.settings restore];
        NSMutableArray *achievedAchievements = [[NSMutableArray alloc]init];
        for (NSString *achievementName in self.settings.achievements) {
            Achievement * achievement = [self achievementByName:achievementName];
            if (achievement) {
                [achievedAchievements addObject:achievement];
            }
        }
        NSLog(@"Settings changed notification fired");
        self.achieved = achievedAchievements;
    }

}
-(id)init
{
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(settingsChangedNotificationRecieved:)
                                                     name:@"SettingsChangedNotification"
                                                   object:nil];

    }
    return self;
}
+(id)sharedAchievementsManager
{
    static AchievementsManager *achievementManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        achievementManager = [[self alloc] init];
    });
    
    return achievementManager;
}
@end
