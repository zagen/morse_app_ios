//
//  PersonalRecordAchievement.m
//  MorseApp
//
//  Created by Admin on 25.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "PersonalRecordAchievement.h"
#import "Settings.h"

@interface PersonalRecordAchievement ()
@property (strong, nonatomic) Settings * settings;
@end

@implementation PersonalRecordAchievement
-(instancetype)init
{
    if (self = [super init]){
        [self.settings restore];
    }
    return  self;
}

-(NSString *)name
{
    return @"Personal record";
}

-(int)showPriority
{
    return 100;
}
-(NSString*)description
{
    return [NSString stringWithFormat:@"You\'re transmited %d characters in a minute", self.settings.personalRecord];
}

-(BOOL)fitToConditios:(NSArray *)resultData
{
    [self countCharacters:resultData];
    
    int personalBest = self.settings.personalRecord;
    
    if(self.countOfCorrectInputedChars > personalBest)
    {
        return  YES;
    }
    return NO;
}
-(NSString *)imageName
{
    return @"ach_record";
}
-(Settings*)settings
{
    if (!_settings) {
        _settings = [Settings sharedSettings];
    }
    return _settings;
}
@end
