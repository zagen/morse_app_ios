//
//  ApprenticeAchievement.m
//  MorseApp
//
//  Created by Admin on 25.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "ApprenticeAchievement.h"

@implementation ApprenticeAchievement

-(NSString *)name
{
    return @"Apprentice";
}
-(int)showPriority
{
    return 80;
}
-(NSString *)description
{
    return @"Trophy for correct transmission more than 15 characters";
}
-(NSString *)imageName
{
    return @"ach_submaster";
}
-(BOOL)fitToConditios:(NSArray *)resultData
{
    [self countCharacters:resultData];
    if( self.countOfCorrectInputedChars >=15)
    {
        return YES;
    }
    return NO;
}
@end
