//
//  NewbieAchievement.m
//  MorseApp
//
//  Created by Admin on 25.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "NewbieAchievement.h"

@implementation NewbieAchievement



-(NSString *)name
{
    return @"Newbie";
}
-(int)showPriority
{
    return 90;
}
-(NSString * )description{
    return @"Trophy for your first time 5 correct transmited characters";
}
-(NSString *)imageName{
    return @"ach_newbie";
}
-(BOOL) fitToConditios:(NSArray *)resultData
{
    [self countCharacters:resultData];
   

    return self.countOfCorrectInputedChars >= 5;
}
@end
