//
//  Achievement.h
//  MorseApp
//
//  Created by Admin on 22.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Achievement : NSObject
@property (strong, nonatomic, readonly) NSString * name;
@property (strong, nonatomic, readonly) NSString * description;
@property (strong, nonatomic, readonly) NSString * imageName;
@property (nonatomic, readonly) int countOfCorrectInputedChars;
@property (nonatomic, readonly) int countOfTotalInputedChars;
@property (nonatomic, readonly) int showPriority;

-(BOOL)fitToConditios:(NSArray *)resultData;
-(void)countCharacters:(NSArray*)resultData;
@end
