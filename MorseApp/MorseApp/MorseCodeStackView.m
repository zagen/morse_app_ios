//
//  MorseCodeStackView.m
//  MorseApp
//
//  Created by Admin on 16.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "MorseCodeStackView.h"
@interface MorseCodeStackView()
@property (strong, nonatomic) NSMutableArray* stack;//of NSStrings

@end
@implementation MorseCodeStackView

- (id)initWithCoder:(NSCoder*)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.symbolSize = 0;
    }
    
    return self;
}


-(NSMutableArray *) stack
{
    if(!_stack){
        _stack = [[NSMutableArray alloc]init];
    }
    return _stack;
}
-(void)addMorseCode:(NSString *)morseCode
{
    int offset = (int)[self.stack count];
    
    for(int i = 0; i < [morseCode length]; i++)
    {
        unichar currentChar = [morseCode characterAtIndex:i];
        self.stack[offset + i] = [NSString
                         stringWithCharacters:&currentChar
                         length:1];
    }
    [self setNeedsDisplay];

}

-(void)clear
{
    [self.stack removeAllObjects];
    [self setNeedsDisplay];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
  
    int width = rect.size.width;
    int height = /*self.symbolSize * 3*/rect.size.height;

    int symbolsHeight = /*self.symbolSize*/	height / 7;
    int dashWidth = symbolsHeight * 3;
    int dotWidth = symbolsHeight;
    int marginVertical = height * 3 / 7/*self.symbolSize*/;
    int marginBetweenTwoSymbols = symbolsHeight;
    
    if (self.symbolSize != 0) {
        width = rect.size.width;
//        height = self.symbolSize * 3;
        
        symbolsHeight = self.symbolSize	;
        dashWidth = symbolsHeight * 3;
        dotWidth = symbolsHeight;
        marginVertical = self.symbolSize;
        marginBetweenTwoSymbols = symbolsHeight;
    }
    

    int posX = width;
    int posY = marginVertical;
    float alpha = 1.0f;
    
    for(int i = (int)[self.stack count] - 1; i >= 0 ; i--){
        if(posX >= 0)
        {
            
            if([self.stack[i] isEqualToString:@"-"])
            {
                posX -= marginBetweenTwoSymbols;
                posX -= dashWidth;
                
                UIBezierPath * path = [[UIBezierPath alloc]init];
                [path moveToPoint:CGPointMake(posX, posY)];
                [path addLineToPoint:CGPointMake(posX + dashWidth, posY)];
                [path addLineToPoint:CGPointMake(posX + dashWidth, posY + symbolsHeight)];
                [path addLineToPoint:CGPointMake(posX , posY + symbolsHeight)];
                [path closePath];
                [[UIColor whiteColor] setFill];
                [path fillWithBlendMode:kCGBlendModeNormal alpha:alpha];
                
               
            }else if([self.stack[i] isEqualToString:@"."])
            {
                posX -= marginBetweenTwoSymbols;
                posX -= dotWidth;
                UIBezierPath *dot = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(posX, posY, dotWidth, symbolsHeight)];
                [[UIColor whiteColor] setFill];
                [dot fillWithBlendMode:kCGBlendModeNormal alpha:alpha];
            }else if([self.stack[i] isEqualToString:@" "])
            {
                posX -= marginBetweenTwoSymbols * 3;
            }else{
                posX -= marginBetweenTwoSymbols * 5;
            }
            alpha = posX / (float)width;

        }else {
            //remove needless unvisible first symbols
            for (int j = i-1; j >= 0; j--) {
                [self.stack removeObjectAtIndex:j];
            }
            break;
        }
       

    }
    
}

- (CGSize)intrinsicContentSize
{
    CGSize size = CGSizeMake(self.frame.size.width, self.symbolSize * 3);
    return size;
}

-(NSString *)stringValue
{
    return [self.stack componentsJoinedByString:@""];
}
@end
