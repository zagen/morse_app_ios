//
//  MorseAppSelectTextFileVC.h
//  MorseApp
//
//  Created by Admin on 03.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MorseAppSelectTextFileDelegate.h"

@interface MorseAppSelectTextFileVC : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) id<MorseAppSelectTextFileDelegate>delegate;
@end
