//
//  SwitcherCellDelegate.h
//  MorseApp
//
//  Created by Admin on 22.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SwitcherCellDelegate <NSObject>
@required
-(void)checkStateChanged:(BOOL)state;
@end
