//
//  MorseAppAppDelegate.h
//  MorseApp
//
//  Created by Admin on 18.05.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MorseAppAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
