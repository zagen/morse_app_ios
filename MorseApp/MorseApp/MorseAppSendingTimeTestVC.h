//
//  MorseAppSendingTimeTestVC.h
//  MorseApp
//
//  Created by Admin on 07.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TouchPadViewDelegate.h"

@interface MorseAppSendingTimeTestVC : UIViewController<UIAlertViewDelegate, TouchPadViewDelegate>

@end
