//
//  MorseAppChoosingSendingExerciseVC.m
//  MorseApp
//
//  Created by Admin on 02.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "MorseAppChooseSendingVC.h"

@interface MorseAppChooseSendingVC ()
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@end

@implementation MorseAppChooseSendingVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.backButton.layer.cornerRadius = self.backButton.frame.size.width/2;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)backToMainMenu:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
