//
//  MorseAppHandbookDetailsVC.h
//  MorseApp
//
//  Created by Admin on 22.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Handbook.h"

@interface MorseAppHandbookDetailsVC : UIViewController
@property (strong, nonatomic) Handbook * handbook;
@property (nonatomic) int indexOfDisplayedCharacter;
@end
