//
//  MorseAppHandbookListVC.m
//  MorseApp
//
//  Created by Admin on 22.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "MorseAppHandbookListVC.h"
#import "MorseAppHandbookDetailsVC.h"
#import "HandbookTableCell.h"
#import "Handbook.h"

@interface MorseAppHandbookListVC ()
@property (weak, nonatomic) IBOutlet UIButton *returnButton;

@property (weak, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) Handbook * handbook;

@property (nonatomic) int selectedIndex;

@end

@implementation MorseAppHandbookListVC

-(Handbook* )handbook{
    if (!_handbook)
    {
        _handbook = [[Handbook alloc] init];
    }
    return _handbook;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.returnButton.layer.cornerRadius = self.returnButton.frame.size.width/2;
    [self.table setDataSource:self];
    [self.table setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)back:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.handbook charactersCount];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HandbookTableCell * cell = [self.table dequeueReusableCellWithIdentifier:@"HandbookListCell"];
    cell.text = [self.handbook characterAtIndex:(int)indexPath.row];
    return cell;
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndex = (int)indexPath.row;
    [self performSegueWithIdentifier:@"handbookDetailsSegue" sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
     if( [[segue identifier] isEqualToString:@"handbookDetailsSegue"])
     {
         MorseAppHandbookDetailsVC * handbookDetailsVC = [segue destinationViewController];
         handbookDetailsVC.handbook = self.handbook;
         handbookDetailsVC.indexOfDisplayedCharacter = self.selectedIndex;
     }
 }

@end
