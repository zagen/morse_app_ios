//
//  MorseAppSendingTimeTestVC.m
//  MorseApp
//
//  Created by Admin on 07.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "MorseAppSendingTimeTestVC.h"
#import "MorseCodeStackView.h"
#import "TouchPadView.h"
#import "General/UILabel+ShowPopup.h"
#import "General/Generator.h"
#import "MorseAppResultVC.h"
#import "MorseAppAchievementsVC.h"
#import "Achievements/AchievementsManager.h"

@interface MorseAppSendingTimeTestVC ()
@property (weak, nonatomic) IBOutlet UIButton *generalView;
@property (weak, nonatomic) IBOutlet UILabel *currentCountLabel;
@property (weak, nonatomic) IBOutlet MorseCodeStackView *morseView;
@property (weak, nonatomic) IBOutlet TouchPadView *touchPad;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeResultLabel;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (nonatomic, getter = isTestInProgress) BOOL testInProgress;
@property (strong, nonatomic) NSMutableArray * resultData;
@property (nonatomic) int countRight;
@property (nonatomic) int countTotal;
@property (strong, nonatomic) NSTimer * timer;
@property (nonatomic) int currentTime;

@property (strong, nonatomic) NSString * currentText;
@property (strong, nonatomic) NSMutableString *currentSymbolMorseCode;
@property (strong, nonatomic) Generator * generator;
@property (strong, nonatomic) TextCodec * codec;
@property (strong, nonatomic) NSString * backupABC;

@end

@implementation MorseAppSendingTimeTestVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.backButton.layer.cornerRadius = self.backButton.bounds.size.width/2;
    self.generalView.layer.cornerRadius = 15;
    self.morseView.symbolSize = 7;
    self.touchPad.enabled = NO;
    [self.statusLabel showPopup:@"For begin tap START"];
    self.touchPad.delegate = self;

        
    //[[Settings sharedSettings] resetToDefaults];
}

-(void)viewWillAppear:(BOOL)animated
{
    Settings * settings = [Settings sharedSettings];
    if (settings.locale == RUS) {
        self.backupABC = settings.currentABCRus;
        settings.currentABCRus = SETTINGS_ABC_CYRILLYC;
    }else{
        self.backupABC = settings.currentABCInt;
        settings.currentABCInt = SETTINGS_ABC_LATIN;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSMutableArray *)resultData
{
    if (!_resultData) {
        _resultData = [[NSMutableArray alloc]init];
    }
    return _resultData;
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (IBAction)start:(id)sender {
    if (!self.isTestInProgress) {
        UIAlertView * view = [[UIAlertView alloc]initWithTitle:@"Are you ready?"
                                                       message:@"You have 1 minute to transmit many symbols you can."
                                                      delegate:self
                                             cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:@"Start", nil];
        [view show];
        
    }
}
-(NSMutableString *)currentSymbolMorseCode{
    if (!_currentSymbolMorseCode) {
        _currentSymbolMorseCode = [[NSMutableString alloc]init];
    }
    return _currentSymbolMorseCode;
}
-(Generator *)generator
{
    if (!_generator) {
        _generator = [[Generator alloc]init];
        _generator.size = 1;
        _generator.sequenceType = CHARS;
    }
    return _generator;
}

-(TextCodec *)codec
{
    if (!_codec) {
        _codec = [[TextCodec alloc]init];
    }
    return _codec;
}


-(void)inputed:(NSString *)symbol
{
    [self.morseView addMorseCode:symbol];
    if ([symbol isEqualToString:@" "]) {
        [self check];
        
        if (!self.isTestInProgress){
            self.touchPad.enabled = NO;
            [self.generalView setTitle:@"RESTART" forState:UIControlStateNormal];
            [self.morseView clear];
            self.currentCountLabel.text = @"Test finished";
            self.timeResultLabel.text = @"Show results";
            self.timeResultLabel.userInteractionEnabled = YES;
            
            if ([[AchievementsManager sharedAchievementsManager] hadSomethingNewAchieved:self.resultData]) {
                [self performSegueWithIdentifier:@"sendingTimeTestVCToAchievementVCSegue" sender:self];
            }
        }else{
            [self newSymbol];
        }
        
    }else{
        if (![symbol isEqualToString:@"   "]) {
            [self.currentSymbolMorseCode appendString:symbol];
        }
    }
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        self.currentTime = 0;
        self.countTotal = 0;
        self.countRight = 0;
        self.resultData = [[NSMutableArray alloc]init];
        self.testInProgress = YES;
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
        self.timeResultLabel.hidden = NO;
        self.timeResultLabel.userInteractionEnabled = NO;
        self.timeResultLabel.text = @"01:00";
        self.touchPad.enabled = YES;
        self.currentCountLabel.hidden = NO;
        self.currentCountLabel.text = [NSString stringWithFormat:@"%i/%i", self.countRight, self.countTotal];
        [self newSymbol];


    }

}

-(void)timerFired:(NSTimer *)timer{
    self.currentTime++;
    if (self.currentTime < 60) {
        self.timeResultLabel.text = [NSString stringWithFormat:@"00:%02i", (60 - self.currentTime)];
    }else{
        self.testInProgress = NO;
        self.timeResultLabel.text = @"00:00";
        [self.timer invalidate];
        self.timer = nil;
    }
}
-(void)newSymbol
{
    self.currentText = [self.generator next];
    [self.generalView setTitle:self.currentText forState:UIControlStateNormal];
    [self.morseView clear];
    [self.currentSymbolMorseCode setString:@""];
    NSLog(@"New symbol: %@", self.currentText);
}

-(void)check
{
    [self.codec setMorse:self.currentSymbolMorseCode];
    NSString * correctText = [self.currentText stringByReplacingOccurrencesOfString:@"Ё" withString:@"E"];
    if ([correctText isEqualToString:self.codec.decodedText]) {
        self.countRight++;
    }
    [self.resultData addObject:@{correctText : self.codec.decodedText}];
    self.countTotal++;
    self.currentCountLabel.text = [NSString stringWithFormat:@"%i/%i", self.countRight, self.countTotal];
}
- (IBAction)showResults:(id)sender {
    if (!self.isTestInProgress && [self.resultData count]) {
        [self performSegueWithIdentifier:@"sendingTimeTestVCtoResultSegue" sender:self];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier ] isEqualToString:@"sendingTimeTestVCtoResultSegue"]) {
        MorseAppResultVC *resultVC = [segue destinationViewController];
        resultVC.resultsData = [self.resultData copy];
        resultVC.returnToPreviousViewController = YES;
    }else if ([[segue identifier] isEqualToString:@"sendingTimeTestVCToAchievementVCSegue"])
    {
        if ([[segue destinationViewController] isKindOfClass:[MorseAppAchievementsVC class]]) {
            MorseAppAchievementsVC *achievementsVC = [segue destinationViewController];
            achievementsVC.resultData = [[self resultData]copy];
        }
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    Settings * settings = [Settings sharedSettings];
    if (settings.locale == RUS) {
        settings.currentABCRus = self.backupABC;
    }else{
        settings.currentABCInt = self.backupABC;
    }
}
@end
