//
//  GrandMasterAchievement.m
//  MorseApp
//
//  Created by Admin on 25.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "GrandMasterAchievement.h"

@implementation GrandMasterAchievement

-(NSString *)name
{
    return @"Grandmaster";
}

-(int)showPriority
{
    return 60;
}
-(NSString*)description
{
    return @"Trophy for correct transmission more than 50 characters";
}

-(BOOL)fitToConditios:(NSArray *)resultData
{
    [self countCharacters:resultData];
    if( self.countOfCorrectInputedChars >= 50)
    {
        return  YES;
    }
    return NO;
}

-(NSString *)imageName
{
    return @"ach_submaster";
}
@end
