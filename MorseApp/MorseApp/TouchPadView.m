//
//  TouchPadView.m
//  MorseApp
//
//  Created by Admin on 05.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "TouchPadView.h"
#import <AVFoundation/AVAudioSession.h>
#import "Settings.h"

OSStatus RenderToneTouchPad(
                    void *inRefCon,
                    AudioUnitRenderActionFlags 	*ioActionFlags,
                    const AudioTimeStamp 		*inTimeStamp,
                    UInt32 						inBusNumber,
                    UInt32 						inNumberFrames,
                    AudioBufferList 			*ioData)

{
	// Fixed amplitude is good enough for our purposes
	const double amplitude = 0.5;
    
	// Get the tone parameters out of the view controller
	TouchPadView *touchPadView =
    (__bridge TouchPadView *)inRefCon;
	double theta = touchPadView->theta;
	double theta_increment = 2.0 * M_PI * touchPadView->frequency / touchPadView->sampleRate;
    
	// This is a mono tone generator so we only need the first buffer
	const int channel = 0;
	Float32 *buffer = (Float32 *)ioData->mBuffers[channel].mData;
	
    float fadeFactor = 1;
	// Generate the samples
	for (UInt32 frame = 0; frame < inNumberFrames; frame++)
	{
        if (touchPadView.isFadeIn) {
            if (touchPadView.position < touchPadView.fadeLength) {
                fadeFactor = (float)(touchPadView.position)/ touchPadView.fadeLength;
                touchPadView.position++;
            }else{
                fadeFactor = 1;
                touchPadView.position = 0;
                touchPadView.isFadeIn = NO;
            }
        }else if(touchPadView.isFadeOut)
        {
            if (touchPadView.position < touchPadView.fadeLength) {
                fadeFactor = (float)(touchPadView.fadeLength - touchPadView.position)/ touchPadView.fadeLength;
                touchPadView.position++;
            }else{
                fadeFactor = 1;
                
                touchPadView.position = 0;
                touchPadView.isFadeOut = NO;
                touchPadView.isSignal = NO;
               
                //need to update our drawing but only in main thread
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [touchPadView setNeedsDisplay];
                }];

            }
        }
        if (touchPadView.isSignal) {
            buffer[frame] = sin(theta) * amplitude * fadeFactor;
        }else{
            buffer[frame] = 0;
        }
        
		theta += theta_increment;
		if (theta > 2.0 * M_PI)
		{
			theta -= 2.0 * M_PI;
		}
        
 }
	
	// Store the theta back in the view controller
	touchPadView->theta = theta;
    
	return noErr;
}
@interface TouchPadView()

@property (nonatomic) float padding;
@property (strong, nonatomic) Settings * settings;
@property (strong, nonatomic) NSTimer * spaceDetectionTimer;
@property (strong, nonatomic) NSTimer * longSpaceDetectionTimer;
@property (nonatomic) float duration;
@property (strong, nonatomic) NSDate *last;
@property (nonatomic) CGRect circleRect;
@property (nonatomic) float radius;
@property (nonatomic) CGPoint centerOfCircle;

@end
@implementation TouchPadView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder*)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self initialize];
    }
    
    return self;
}
-(void)initialize
{
    frequency = self.settings.toneFrequence;
    sampleRate = 44100;
    
    NSError *audioSessionError = nil;
    AVAudioSession *session = [AVAudioSession sharedInstance];     // 1
    
    [session setCategory: AVAudioSessionCategoryAmbient      // 3
                   error: &audioSessionError];
    [session setActive: YES
                 error: &audioSessionError];
    [session setPreferredSampleRate:sampleRate error:nil];
    sampleRate = [session sampleRate];
    int WPM = self.settings.transmitterSpeed;
    self.duration = ceil(1200.0/WPM );
    
    int dotSamplesCount = (self.duration * sampleRate)/1000;
    self.fadeLength = dotSamplesCount * 0.15;

    self.isSignal = NO;
    [self play];
    
    CGFloat height = self.bounds.size.height;
    CGFloat width = self.bounds.size.width;
    CGFloat originX = self.bounds.origin.x;
    CGFloat originY = self.bounds.origin.y;
    
    CGFloat minSide = (height > width) ? width : height;
    minSide -= self.padding * 2;
    self.circleRect  = CGRectMake(
                                    originX + width/2 - minSide/2,
                                    originY + height/2 - minSide/2,
                                    minSide,
                                    minSide);

    self.radius = minSide/2;
    self.centerOfCircle = CGPointMake(self.circleRect.origin.x + self.radius,
                              self.circleRect.origin.y + self.radius);
    
    self.enabled = YES;

}

-(float)padding
{
    return 5;
}


-(void)drawRect:(CGRect)rect
{
   
    
    UIBezierPath * path = [UIBezierPath bezierPathWithOvalInRect:self.circleRect];
    [[UIColor whiteColor] setStroke];
    if (self.isSignal) {
        [[UIColor whiteColor]setFill];
    }else{
        [[UIColor blackColor]setFill];
    }
    path.lineWidth = 3;
    [path stroke];
    [path fill];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ([touches count] == 1 && self.isEnabled) {
        UITouch * touch = [touches anyObject];
        CGPoint pos = [touch locationInView: self];
        float distance = sqrt(pow(self.centerOfCircle.x - pos.x, 2) + pow(self.centerOfCircle.y - pos.y, 2));
     
        if (distance <= self.radius)
        {
            if (self.spaceDetectionTimer) {
                [self.spaceDetectionTimer invalidate];
                self.spaceDetectionTimer = nil;
            }
            if (self.longSpaceDetectionTimer) {
                [self.longSpaceDetectionTimer invalidate];
                self.longSpaceDetectionTimer = nil;
            }

            self.isSignal = YES;
            self.isFadeIn = YES;
            [self setNeedsDisplay];
            self.last = [NSDate date];
        }
    }
    
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{

    if (self.last) {
        NSTimeInterval interval = [self.last timeIntervalSinceNow] * -1000;
        if ( self.delegate ) {
            if (interval < self.duration * 3) {
                [self.delegate inputed:@"."];
            }else{
                [self.delegate inputed:@"-"];
            }
        }
        self.last = nil;
    
        if(self.isEnabled){
            self.spaceDetectionTimer = [NSTimer scheduledTimerWithTimeInterval:self.duration * 3/1000.0 target:self selector:@selector(spaceTimerFired:) userInfo:nil repeats:NO];
        }
        self.isFadeOut = YES;
    }
}

-(Settings *)settings
{
    if (!_settings) {
        _settings = [Settings sharedSettings];
    }
    return _settings;
}


- (void)createToneUnit
{
	// Configure the search parameters to find the default playback output unit
	// (called the kAudioUnitSubType_RemoteIO on iOS but
	// kAudioUnitSubType_DefaultOutput on Mac OS X)
	AudioComponentDescription defaultOutputDescription;
	defaultOutputDescription.componentType = kAudioUnitType_Output;
	defaultOutputDescription.componentSubType = kAudioUnitSubType_RemoteIO;
	defaultOutputDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
	defaultOutputDescription.componentFlags = 0;
	defaultOutputDescription.componentFlagsMask = 0;
	
	// Get the default playback output unit
	AudioComponent defaultOutput = AudioComponentFindNext(NULL, &defaultOutputDescription);
	NSAssert(defaultOutput, @"Can't find default output");
	
	// Create a new unit based on this that we'll use for output
	OSErr err = AudioComponentInstanceNew(defaultOutput, &toneUnit);
	NSAssert1(toneUnit, @"Error creating unit: %hd", err);
	
	// Set our tone rendering function on the unit
	AURenderCallbackStruct input;
	input.inputProc = RenderToneTouchPad;
	input.inputProcRefCon = (__bridge void *)(self);
	err = AudioUnitSetProperty(toneUnit,
                               kAudioUnitProperty_SetRenderCallback,
                               kAudioUnitScope_Input,
                               0,
                               &input,
                               sizeof(input));
	NSAssert1(err == noErr, @"Error setting callback: %hd", err);
	
	// Set the format to 32 bit, single channel, floating point, linear PCM
	const int four_bytes_per_float = 4;
	const int eight_bits_per_byte = 8;
	AudioStreamBasicDescription streamFormat;
	streamFormat.mSampleRate = sampleRate;
	streamFormat.mFormatID = kAudioFormatLinearPCM;
	streamFormat.mFormatFlags =
    kAudioFormatFlagsNativeFloatPacked | kAudioFormatFlagIsNonInterleaved;
	streamFormat.mBytesPerPacket = four_bytes_per_float;
	streamFormat.mFramesPerPacket = 1;
	streamFormat.mBytesPerFrame = four_bytes_per_float;
	streamFormat.mChannelsPerFrame = 1;
	streamFormat.mBitsPerChannel = four_bytes_per_float * eight_bits_per_byte;
	err = AudioUnitSetProperty (toneUnit,
                                kAudioUnitProperty_StreamFormat,
                                kAudioUnitScope_Input,
                                0,
                                &streamFormat,
                                sizeof(AudioStreamBasicDescription));
	NSAssert1(err == noErr, @"Error setting stream format: %hd", err);
}
-(void)stop
{
    if (toneUnit)
	{
		AudioOutputUnitStop(toneUnit);
		AudioUnitUninitialize(toneUnit);
		AudioComponentInstanceDispose(toneUnit);
		toneUnit = nil;
    }
}
-(void)play
{
    if (!toneUnit) {
        [self createToneUnit];
        
        // Stop changing parameters on the unit
        OSErr err = AudioUnitInitialize(toneUnit);
        NSAssert1(err == noErr, @"Error initializing unit: %hd", err);
        
        // Start playback
        err = AudioOutputUnitStart(toneUnit);
        NSAssert1(err == noErr, @"Error starting unit: %hd", err);
    }
}

-(void)spaceTimerFired:(NSTimer *)timer
{
    if (self.delegate) {
        if (timer == self.spaceDetectionTimer) {
            [self.delegate inputed:@" "];
            self.longSpaceDetectionTimer = [NSTimer scheduledTimerWithTimeInterval:self.duration * 4 / 1000.0 target:self selector:@selector(spaceTimerFired:) userInfo:nil repeats:NO];
        }else if(timer == self.longSpaceDetectionTimer)
        {
            [self.delegate inputed:@"   "];
        }
    }
    NSLog(@"Timer fired");
    self.spaceDetectionTimer = nil;
    
}

-(void)dealloc{
    [self stop];
}
@end
