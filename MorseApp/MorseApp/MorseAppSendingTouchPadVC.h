//
//  MorseAppSendingTouchPadVC.h
//  MorseApp
//
//  Created by Admin on 05.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TouchPadViewDelegate.h"

@interface MorseAppSendingTouchPadVC : UIViewController<TouchPadViewDelegate>

@end
