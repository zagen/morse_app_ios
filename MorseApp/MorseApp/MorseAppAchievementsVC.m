//
//  MorseAppAchievementsVC.m
//  MorseApp
//
//  Created by Admin on 26.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "MorseAppAchievementsVC.h"
#import "Achievements/AchievementsManager.h"
#import "Achievements/Achievement.h"

@interface MorseAppAchievementsVC ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *achievementTitle;
@property (weak, nonatomic) IBOutlet UILabel *achievementDescription;
@property (weak, nonatomic) IBOutlet UIButton *returnButton;
@property (weak, nonatomic) IBOutlet UILabel *viewTitle;

@property (nonatomic)BOOL isNewShowing;
@property (strong, nonatomic) NSArray * achievements;//of Achievement
@property (nonatomic)int currentAchievementIndex;

@property (strong, nonatomic, readonly) NSString * appUrl;

@end

@implementation MorseAppAchievementsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.returnButton.layer.cornerRadius = self.returnButton.frame.size.width/2;
    AchievementsManager * manager = [AchievementsManager sharedAchievementsManager];

    if (self.resultData) {
        self.achievements = [manager analyze:self.resultData];
        self.isNewShowing = true;
    }else{
        self.isNewShowing = false;
        self.achievements = manager.achieved;
    }
    [self updateUIwithImageAnimation:UIViewAnimationOptionTransitionCurlDown];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)updateUIwithImageAnimation:(UIViewAnimationOptions) options
{
    double animationDuration = 0.5;
    if ([self.achievements count]) {
        Achievement * currentAchievement = self.achievements[self.currentAchievementIndex];
        self.achievementTitle.text = currentAchievement.name;
        self.imageView.image = [UIImage imageNamed:currentAchievement.imageName];
        self.achievementDescription.text = currentAchievement.description;
        [UIView transitionWithView:self.imageView
                          duration:animationDuration
                           options:UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{
                                        self.imageView.image = [UIImage imageNamed:currentAchievement.imageName];
                                    }
                        completion:^(BOOL completed){}];
        [UIView animateWithDuration:animationDuration/2
                         animations:^{
                             self.achievementTitle.alpha = 0;
                             self.achievementDescription.alpha = 0;
                             }
                         completion:^(BOOL finished){
                             self.achievementTitle.alpha = 0;
                             self.achievementDescription.alpha = 0;
                             if (!self.isNewShowing) {
                                 self.viewTitle.text = [NSString stringWithFormat:@"Achievements:  %d from %d", self.currentAchievementIndex + 1, [self.achievements count]];
                             }else{
                                 self.viewTitle.text = [NSString stringWithFormat:@"New achievements:  %d from %d", self.currentAchievementIndex + 1, [self.achievements count]];
                             }
                             
                                                    
                             [UIView animateWithDuration:animationDuration/2
                                              animations:^{
                                                  self.achievementTitle.alpha = 1;
                                                  self.achievementDescription.alpha = 1;
                                              }completion:nil];
                         }];

    }
    
}

-(NSString *)appUrl
{
    return @"https://itunes.apple.com/ru/genre/mobile-software-applications/id36?mt=8";
}
- (IBAction)returnToMainMenu:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)swipeLeft:(UISwipeGestureRecognizer *)sender {
    self.currentAchievementIndex = (self.currentAchievementIndex + 1 >= [self.achievements count]) ? 0 : self.currentAchievementIndex + 1;
    [self updateUIwithImageAnimation:UIViewAnimationOptionTransitionFlipFromRight];
}

- (IBAction)swipeRight:(UISwipeGestureRecognizer *)sender {
    self.currentAchievementIndex = (self.currentAchievementIndex - 1 < 0) ? [self.achievements count] - 1 : self.currentAchievementIndex - 1;
    [self updateUIwithImageAnimation:UIViewAnimationOptionTransitionFlipFromLeft];
}
- (IBAction)onLongPressShare:(id)sender {
    if ([self.achievements count]) {
        Achievement * achievement = self.achievements[self.currentAchievementIndex];
        NSString * text = [NSString stringWithFormat:@"In MorseApp I got achievement \"%@\".\nLearn more about app", achievement.name];
        UIActivityViewController *controller =
        [[UIActivityViewController alloc]
         initWithActivityItems:@[text, self.appUrl, [UIImage imageNamed:achievement.imageName]]
         applicationActivities:nil];
        controller.excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                             UIActivityTypeAssignToContact,
                                             UIActivityTypeSaveToCameraRoll,
                                             UIActivityTypeAddToReadingList,
                                             UIActivityTypePostToFlickr,
                                             UIActivityTypePostToVimeo,
                                             UIActivityTypePostToTencentWeibo,
                                             UIActivityTypeAirDrop];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

@end
