//
//  MorseAppSettingsVC.m
//  MorseApp
//
//  Created by Admin on 20.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "MorseAppSettingsVC.h"
#import "SliderCell.h"
#import "SwitcherCell.h"

@interface MorseAppSettingsVC ()
@property (weak, nonatomic) IBOutlet UIButton *returnButton;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) Settings * settings;
@end

@implementation MorseAppSettingsVC

+(NSArray *)transmitters{
    return @[@"Tone"];
}
+(NSArray *)locals{
    return @[@"International", @"Cyrillic"];
}

-(Settings *)settings
{
    if(!_settings)
    {
        _settings = [Settings sharedSettings];
    }
    return  _settings;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.returnButton.layer.cornerRadius = self.returnButton.frame.size.width/2;
    [self.table setDelegate:self];
    [self.table setDataSource:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)returnToMainMenu:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 7;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger number = 1;
    
    if(section == 0){
        number = [[MorseAppSettingsVC locals] count];
    }else if(section == 1)
    {
        number = [[MorseAppSettingsVC transmitters]count];
    }
    
    return number;
}


- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    UILabel *headerView=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    headerView.backgroundColor=[UIColor clearColor];
    headerView.shadowColor = [UIColor darkGrayColor];
    headerView.shadowOffset = CGSizeMake(0,2);
    headerView.textColor = [UIColor whiteColor]; //here you can change the text color of header.

    headerView.font = [UIFont systemFontOfSize:14];
 
    
    NSString *sectionName;
    switch (section)
    {
        case 0:
            sectionName = @"Locale";
            break;
        case 1:
            sectionName = @"Transmitter";
            break;
        case 3:
            sectionName = @"Transmission speed(WPM):";
            break;
        /*case 4:
            sectionName = @"Volume:";
            break;*/
        case 4:
            sectionName = @"Length of generating sequence:";
            break;
        case 5:
            sectionName = @"Groups number on receive:";
            break;
        case 6:
            sectionName = @"Custom tone frequence:";
            break;
        default:
            sectionName = @"";
            break;
    }
    headerView.text= sectionName;
    return  headerView;
}
- (IBAction)didTapOnTableView:(UITapGestureRecognizer *)sender
{
   
    CGPoint tapLocation = [sender locationInView:self.table];
    
    NSIndexPath *indexPath = [self.table indexPathForRowAtPoint:tapLocation];
    
    if (indexPath) { //we are in a tableview cell, let the gesture be handled by the view
        sender.cancelsTouchesInView = NO;
        if(indexPath.section == 0 && indexPath.row != self.settings.locale)
        {
            NSIndexPath * indexPathOldLocale = [NSIndexPath indexPathForRow:self.settings.locale
                                                                  inSection:0];
            UITableViewCell *cell = [self.table cellForRowAtIndexPath:indexPathOldLocale];
            cell.accessoryType = UITableViewCellAccessoryNone;
            self.settings.locale = (int)indexPath.row;
            cell = [self.table cellForRowAtIndexPath:indexPath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [self.settings save];
        }else if (indexPath.section == 1 && indexPath.row != self.settings.transmitter)
        {
            NSIndexPath * indexPathOldTransmitter = [NSIndexPath indexPathForRow:self.settings.transmitter
                                                                  inSection:1];
            UITableViewCell *cell = [self.table cellForRowAtIndexPath:indexPathOldTransmitter];
            cell.accessoryType = UITableViewCellAccessoryNone;
            self.settings.transmitter = (int)indexPath.row;
            cell = [self.table cellForRowAtIndexPath:indexPath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [self.settings save];
        }
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //before all this we have to receive all settings from user defaults and init our UI in accordance to its
    
    static NSString *CellIdentifierCheckbox = @"SettingsCheckBox";
    static NSString *CellIdentifierSlider   = @"SettingsSlider";
    static NSString *CellIdentifierSwitcher = @"SettingsSwitcher";
    
    UITableViewCell * cell;
    
    if(indexPath.section < 2){
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierCheckbox];
        cell.accessoryType = UITableViewCellAccessoryNone;
        if(indexPath.section == 0)
        {
            cell.textLabel.text = [[MorseAppSettingsVC locals] objectAtIndex:indexPath.row];
            
            if(self.settings.locale == indexPath.row)
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
        }else
        {
            cell.textLabel.text = [[MorseAppSettingsVC transmitters] objectAtIndex: indexPath.row];
            
            if(self.settings.transmitter == indexPath.row)
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
        }
    }else if( indexPath.section == 2)
    {
        SwitcherCell * cellWithSwitcher = [tableView dequeueReusableCellWithIdentifier:CellIdentifierSwitcher];
        
        BOOL switcherValue = (self.settings.isAnimatedHelpOn == 1);
        [cellWithSwitcher initWithLabelText:@"Animated help text is on" andSwitchValue:switcherValue];
        cellWithSwitcher.delegate = self;
        cell = cellWithSwitcher;
    }else
    {
        SliderCell * cellWithSlider;
        cellWithSlider = [tableView dequeueReusableCellWithIdentifier:CellIdentifierSlider];
        cellWithSlider.path = indexPath;
        cellWithSlider.delegate = self;
        cell = cellWithSlider;
        switch (indexPath.section) {

            case 3:
                [cellWithSlider initSliderValues:NSMakeRange(5, 25)
                                         beginAt:self.settings.transmitterSpeed
                                            step:1];
                break;
            /*case 4:
                [cellWithSlider initSliderValues:NSMakeRange(0, 15)
                                         beginAt:self.settings.volume
                                            step:1];
                
                break;*/
            case 4:
                [cellWithSlider initSliderValues:NSMakeRange(1, 9)
                                         beginAt:self.settings.generatorSize
                                            step:1];
                
                break;
            case 5:
                [cellWithSlider initSliderValues:NSMakeRange(1, 29)
                                         beginAt:self.settings.groupsNumber
                                            step:1];
                
                break;
            case 6:
                [cellWithSlider initSliderValues:NSMakeRange(400, 1600)
                                         beginAt:self.settings.toneFrequence
                                            step:20];
                
                break;
                
            default:
                break;
        }
    }
    return cell;
}

-(void)slidingDidStop:(NSIndexPath *)path atValue:(float)value
{
    NSLog(@"Here ok");
    switch (path.section) {
            
        case 3:
            self.settings.transmitterSpeed = (int)value;
            break;
        /*case 4:
            self.settings.volume = (int)value;
            break;*/
        case 4:
            self.settings.generatorSize = (int)value;
            break;
        case 5:
            self.settings.groupsNumber = (int)value;
            break;
        case 6:
            self.settings.toneFrequence = (int)value;
            break;
            
        default:
            break;
    }
    [self.settings save];
}

-(void)checkStateChanged:(BOOL)state
{
    int value = state;
    self.settings.isAnimatedHelpOn = value;
    [self.settings save];
}
@end
