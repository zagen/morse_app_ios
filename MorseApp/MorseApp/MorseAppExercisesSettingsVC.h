//
//  MorseAppExercisesSettingsVC.h
//  MorseApp
//
//  Created by Admin on 14.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SliderCellDelegate.h"

@interface MorseAppExercisesSettingsVC : UIViewController<UITableViewDataSource, UITableViewDelegate, SliderCellDelegate>
@property (nonatomic, getter = isReceiveExercise) BOOL receiveExercise;
@end
