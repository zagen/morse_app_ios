//
//  SliderCellDelegate.h
//  MorseApp
//
//  Created by Admin on 21.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SliderCellDelegate <NSObject>
@required
-(void)slidingDidStop:(NSIndexPath *)path atValue:(float)value;
@end
