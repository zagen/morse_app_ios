//
//  MorseAppSelectTextFileDelegate.h
//  MorseApp
//
//  Created by Admin on 04.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MorseAppSelectTextFileDelegate <NSObject>
-(void)selectedTextFileWithName:(NSString* )filename;
@end
