//
//  HandbookTableCell.h
//  MorseApp
//
//  Created by Admin on 22.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HandbookTableCell : UITableViewCell
@property (strong,nonatomic) NSString * text;
@end
