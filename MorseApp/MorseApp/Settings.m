//
//  Settings.m
//  MorseApp
//
//  Created by Admin on 21.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "Settings.h"
#import "General/TextCodec.h"

NSString * const SETTINGS_LOCALE_INT = @"Locale";
NSString * const SETTINGS_TRANSMITTER_INT = @"Transmitter";
NSString * const SETTINGS_ANIMATION_HELP_ON_INT = @"AnimationHelpIsOn";
NSString * const SETTINGS_TRANSMISSION_SPEED_INT = @"TransmissionSpeed";
NSString * const SETTINGS_VOLUME_INT = @"Volume";
NSString * const SETTINGS_GENERATOR_SIZE_INT = @"GeneratorSize";
NSString * const SETTINGS_GROUPS_NUMBER_INT = @"GroupsNumber";
NSString * const SETTINGS_TONE_FREQUENCE_INT = @"ToneFrequence";
NSString * const SETTINGS_PERSONAL_RECORD_INT = @"PersonalRecord";
NSString * const SETTINGS_ACHIEVEMENTS_ARRAY_OF_NAMES = @"Achievements";
NSString * const SETTINGS_GENERATOR_SEQUENCE_TYPE_INT = @"SequenceType";
NSString * const SETTINGS_TEXT_FILENAME_STRING = @"TextFilename";
NSString * const SETTINGS_TEXT_CURRENTPOSITION_INT = @"TextCurrentPosition";
NSString * const SETTINGS_GENERATOR_CURRENT_ABC_INTER_STRING = @"GeneratorCurrentABCINT";
NSString * const SETTINGS_GENERATOR_CURRENT_ABC_RUS_STRING = @"GeneratorCurrentABCRUS";
NSString * const SETTINGS_LISTENING_TRANSMISSION_DELAY_INT = @"ListeningTransmissionDelay";


NSString * const SETTINGS_ABC_LATIN = @"abcdefghijklmnopqrstuvwxyz1234567890.,:;()\"'-/?!";
NSString * const SETTINGS_ABC_CYRILLYC = @"абвгдеёжзийклмнопрстуфхцчшщъыьэюя1234567890.,:;()\"'-/?!";

int const SETTINGS_DEFAULT_LOCALE_INT = INT;
int const SETTINGS_DEFAULT_TRANSMITTER_INT = 0;
int const SETTINGS_DEFAULT_ANIMATION_HELP_ON_INT = 1;
int const SETTINGS_DEFAULT_TRANSMISSION_SPEED_INT = 15;
int const SETTINGS_DEFAULT_VOLUME_INT = 5;
int const SETTINGS_DEFAULT_GENERATOR_SIZE_INT = 2;
int const SETTINGS_DEFAULT_GROUPS_NUMBER_INT = 1;
int const SETTINGS_DEFAULT_TONE_FREQUENCE_INT = 1000;
int const SETTINGS_DEFAULT_PERSONAL_RECORD_INT = 0;
NSArray* const SETTINGS_DEFAULT_ACHIEVEMENTS_ARRAY_OF_NAMES = nil;
int const SETTINGS_DEFAULT_GENERATOR_SEQUENCE_TYPE_INT = 0;
NSString * const SETTINGS_DEFAULT_TEXT_FILENAME_STRING = nil;
int const SETTINGS_DEFAULT_TEXT_CURRENTPOSITION_INT = 0;
int const SETTINGS_DEFAULT_LISTENING_TRANSMISSION_DELAY_INT = 0;

@implementation Settings

-(void)save
{
    // Store the data
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setInteger:self.locale forKey:SETTINGS_LOCALE_INT];
    [defaults setInteger:self.transmitter forKey:SETTINGS_TRANSMITTER_INT];
    [defaults setInteger:self.isAnimatedHelpOn forKey:SETTINGS_ANIMATION_HELP_ON_INT];
    [defaults setInteger:self.transmitterSpeed forKey:SETTINGS_TRANSMISSION_SPEED_INT];
    [defaults setInteger:self.generatorSize forKey:SETTINGS_GENERATOR_SIZE_INT];
    [defaults setInteger:self.groupsNumber forKey:SETTINGS_GROUPS_NUMBER_INT];
    [defaults setInteger:self.volume forKey:SETTINGS_VOLUME_INT];
    [defaults setInteger:self.toneFrequence forKey:SETTINGS_TONE_FREQUENCE_INT];
    [defaults setInteger:self.personalRecord forKey:SETTINGS_PERSONAL_RECORD_INT];
    [defaults setInteger:self.sequenceType forKey:SETTINGS_GENERATOR_SEQUENCE_TYPE_INT];
    [defaults setObject:self.achievements forKey:SETTINGS_ACHIEVEMENTS_ARRAY_OF_NAMES];
    [defaults setObject:self.textFilename forKey:SETTINGS_TEXT_FILENAME_STRING];
    [defaults setInteger:self.textCurrentPosition forKey:SETTINGS_TEXT_CURRENTPOSITION_INT];
    [defaults setObject:self.currentABCInt forKey:SETTINGS_GENERATOR_CURRENT_ABC_INTER_STRING];
    [defaults setObject:self.currentABCRus forKey:SETTINGS_GENERATOR_CURRENT_ABC_RUS_STRING];
    [defaults setInteger:self.delayBeforeTransmission forKey:SETTINGS_LISTENING_TRANSMISSION_DELAY_INT];
    [defaults synchronize];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"SettingsChangedNotification" object:self];
    
}
-(void)restore
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    if([defaults objectForKey:SETTINGS_LOCALE_INT])
    {
        self.locale = (int)[defaults integerForKey:SETTINGS_LOCALE_INT];
    }else{
        self.locale = SETTINGS_DEFAULT_LOCALE_INT;
    }
    
    if([defaults objectForKey:SETTINGS_TRANSMITTER_INT])
    {
        self.transmitter = (int)[defaults integerForKey:SETTINGS_TRANSMITTER_INT];
    }else{
        self.transmitter = SETTINGS_DEFAULT_TRANSMITTER_INT;
    }
    
    if([defaults objectForKey:SETTINGS_ANIMATION_HELP_ON_INT])
    {
        self.isAnimatedHelpOn = (int)[defaults integerForKey:SETTINGS_ANIMATION_HELP_ON_INT];
    }else{
        self.isAnimatedHelpOn = SETTINGS_DEFAULT_ANIMATION_HELP_ON_INT;
    }
    
    if([defaults objectForKey:SETTINGS_TRANSMISSION_SPEED_INT])
    {
        self.transmitterSpeed = (int)[defaults integerForKey:SETTINGS_TRANSMISSION_SPEED_INT];
    }else{
        self.transmitterSpeed = SETTINGS_DEFAULT_TRANSMISSION_SPEED_INT;
    }
    
    if([defaults objectForKey:SETTINGS_GENERATOR_SIZE_INT])
    {
        self.generatorSize = (int)[defaults integerForKey:SETTINGS_GENERATOR_SIZE_INT];
    }else{
        self.generatorSize = SETTINGS_DEFAULT_GENERATOR_SIZE_INT;
    }
    
    if([defaults objectForKey:SETTINGS_GROUPS_NUMBER_INT])
    {
        self.groupsNumber = (int)[defaults integerForKey:SETTINGS_GROUPS_NUMBER_INT];
    }else{
        self.groupsNumber = SETTINGS_DEFAULT_GROUPS_NUMBER_INT;
    }
  
    if([defaults objectForKey:SETTINGS_VOLUME_INT])
    {
        self.volume = (int)[defaults integerForKey:SETTINGS_VOLUME_INT];
    }else{
        self.volume = SETTINGS_DEFAULT_VOLUME_INT;
    }
    
    if([defaults objectForKey:SETTINGS_TONE_FREQUENCE_INT])
    {
        self.toneFrequence = (int)[defaults integerForKey:SETTINGS_TONE_FREQUENCE_INT];
    }else{
        self.toneFrequence = SETTINGS_DEFAULT_TONE_FREQUENCE_INT;
    }
    
    if([defaults objectForKey:SETTINGS_PERSONAL_RECORD_INT])
    {
        self.personalRecord = (int)[defaults integerForKey:SETTINGS_PERSONAL_RECORD_INT];
    }else{
        self.personalRecord = SETTINGS_DEFAULT_PERSONAL_RECORD_INT;
    }
    
    if([defaults objectForKey:SETTINGS_PERSONAL_RECORD_INT])
    {
        self.achievements = [defaults arrayForKey:SETTINGS_ACHIEVEMENTS_ARRAY_OF_NAMES];
    }else{
        self.achievements = SETTINGS_DEFAULT_ACHIEVEMENTS_ARRAY_OF_NAMES;
    }
    if([defaults objectForKey:SETTINGS_GENERATOR_SEQUENCE_TYPE_INT])
    {
        self.sequenceType = (int)[defaults integerForKey:SETTINGS_GENERATOR_SEQUENCE_TYPE_INT];
    }else{
        self.sequenceType = SETTINGS_DEFAULT_GENERATOR_SEQUENCE_TYPE_INT;
    }
    
    if([defaults objectForKey:SETTINGS_TEXT_FILENAME_STRING])
    {
        self.textFilename = [defaults objectForKey:SETTINGS_TEXT_FILENAME_STRING];
    }else{
        self.textFilename = SETTINGS_DEFAULT_TEXT_FILENAME_STRING;
    }
    
    if([defaults objectForKey:SETTINGS_TEXT_CURRENTPOSITION_INT])
    {
        self.textCurrentPosition = (int)[defaults integerForKey:SETTINGS_TEXT_CURRENTPOSITION_INT];
    }else{
        self.textCurrentPosition = SETTINGS_DEFAULT_TEXT_CURRENTPOSITION_INT;
    }
    
    if([defaults objectForKey:SETTINGS_GENERATOR_CURRENT_ABC_INTER_STRING])
    {
        self.currentABCInt = [defaults objectForKey:SETTINGS_GENERATOR_CURRENT_ABC_INTER_STRING];
    }else{
        self.currentABCInt = SETTINGS_ABC_LATIN;
    }
    
    if([defaults objectForKey:SETTINGS_GENERATOR_CURRENT_ABC_RUS_STRING])
    {
        self.currentABCRus = [defaults objectForKey:SETTINGS_GENERATOR_CURRENT_ABC_RUS_STRING];
    }else{
        self.currentABCRus = SETTINGS_ABC_CYRILLYC;
    }
    if([defaults objectForKey:SETTINGS_LISTENING_TRANSMISSION_DELAY_INT])
    {
        self.delayBeforeTransmission = (int)[defaults integerForKey:SETTINGS_LISTENING_TRANSMISSION_DELAY_INT];
    }else{
        self.delayBeforeTransmission = SETTINGS_DEFAULT_LISTENING_TRANSMISSION_DELAY_INT;
    }
    
}
-(instancetype)init
{
    if(self = [super init]){
        [self restore];
    }
    return self;
}
-(void)resetToDefaults
{
    self.locale = SETTINGS_DEFAULT_LOCALE_INT;
    self.transmitter = SETTINGS_DEFAULT_TRANSMITTER_INT;
    self.isAnimatedHelpOn = SETTINGS_DEFAULT_ANIMATION_HELP_ON_INT;
    self.transmitterSpeed = SETTINGS_DEFAULT_TRANSMISSION_SPEED_INT;
    self.generatorSize = SETTINGS_DEFAULT_GENERATOR_SIZE_INT;
    self.groupsNumber = SETTINGS_DEFAULT_GROUPS_NUMBER_INT;
    self.volume = SETTINGS_DEFAULT_VOLUME_INT;
    self.toneFrequence = SETTINGS_DEFAULT_TONE_FREQUENCE_INT;
    self.personalRecord = SETTINGS_DEFAULT_PERSONAL_RECORD_INT;
    self.achievements = SETTINGS_DEFAULT_ACHIEVEMENTS_ARRAY_OF_NAMES;
    self.sequenceType = SETTINGS_DEFAULT_GENERATOR_SEQUENCE_TYPE_INT;
    self.textFilename = SETTINGS_DEFAULT_TEXT_FILENAME_STRING;
    self.textCurrentPosition = SETTINGS_DEFAULT_TEXT_CURRENTPOSITION_INT;
    self.currentABCRus = SETTINGS_ABC_CYRILLYC;
    self.currentABCInt = SETTINGS_ABC_LATIN;
    self.delayBeforeTransmission = SETTINGS_DEFAULT_LISTENING_TRANSMISSION_DELAY_INT;
    
    [self save];
}
+(id)sharedSettings
{
    static Settings *settings = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        settings = [[self alloc] init];
    });
    
    return settings;
}
@end
