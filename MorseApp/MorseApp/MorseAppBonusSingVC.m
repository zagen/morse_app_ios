//
//  MorseAppBonusSingVC.m
//  MorseApp
//
//  Created by Admin on 26.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "MorseAppBonusSingVC.h"


@interface MorseAppBonusSingVC ()
@property (weak, nonatomic) IBOutlet UIImageView *radioView;
@property (weak, nonatomic) IBOutlet UISlider *progressSlider;
@property (weak, nonatomic) IBOutlet UILabel *currentTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *reloadButton;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (strong,nonatomic) AVAudioPlayer * player;
@property (strong,nonatomic) NSTimer * timer;

@property (strong,nonatomic) NSArray * radioImages;//of UiImage
@end

@implementation MorseAppBonusSingVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(NSArray *)radioImages
{
    if (!_radioImages) {
        _radioImages = @[
                         [UIImage imageNamed:@"radio0"],
                         [UIImage imageNamed:@"radio1"],
                         [UIImage imageNamed:@"radio2"],
                         [UIImage imageNamed:@"radio3"],
                         [UIImage imageNamed:@"radio4"]
                         ];
    }
    return _radioImages;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.reloadButton.layer.cornerRadius = self.reloadButton.frame.size.width/2;
    self.okButton.layer.cornerRadius = self.okButton.frame.size.width/2;
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/bonus.m4a", [[NSBundle mainBundle] resourcePath]]];
    NSError *error;
	self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
	self.player.numberOfLoops = 0;
    [self.player prepareToPlay];
	
	if (self.player == nil){
		NSLog(@"Audio player = nil . Error: %@", [error description]);
    }else{
        self.player.delegate = self;
        double minutes = floor(self.player.duration / 60);
        double seconds= round(self.player.duration - minutes * 60);
        self.totalTimeLabel.text = [NSString stringWithFormat:@"%02.0f:%02.0f", minutes, seconds];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)returnToMainMenu:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
    if ([self.player isPlaying]) {
        [self.player stop];
    }
    [self.timer invalidate];
}


- (IBAction)playOrPause:(UITapGestureRecognizer *)sender {
    if (!self.radioView.isAnimating)
    {
        self.radioView.animationImages = self.radioImages;
        self.radioView.animationDuration = 1;
        [self.radioView startAnimating];
        [self.player prepareToPlay];
        [self.player play];
 
    }else{
        [self.radioView stopAnimating];
        self.radioView.image = self.radioImages[0];
        [self.player pause];
    }
}
-(void)seekOnPosition:(double)position andPlay:(BOOL)isPlay{
    [self.player stop];
    [self.player setCurrentTime: self.player.duration * position];

    if (isPlay) {
        [self.player prepareToPlay];
        [self.player play];
        
        if (!self.radioView.isAnimating) {
            [self.radioView startAnimating];
        }
    }


}

-(void)updateTime:(NSTimer*)timer
{
    if(self.player.currentTime < 0){
        self.player.currentTime = 0;
    }
    double minutes = floor(self.player.currentTime / 60);
    double seconds= round(self.player.currentTime - minutes * 60);
    self.currentTimeLabel.text = [NSString stringWithFormat:@"%02.0f:%02.0f", minutes, seconds];
    
    if (self.player.duration) {
        self.progressSlider.value = self.player.currentTime / self.player.duration;
    }
}
- (IBAction)progressChanged:(UISlider *)sender {

    [self seekOnPosition:sender.value andPlay:YES];
}
- (IBAction)replay:(id)sender {
    [self seekOnPosition:0 andPlay:YES];
}
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [self seekOnPosition:0 andPlay:NO];
    [self.radioView stopAnimating];

}
@end
