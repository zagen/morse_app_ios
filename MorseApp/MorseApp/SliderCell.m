//
//  SliderCell.m
//  MorseApp
//
//  Created by Admin on 21.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "SliderCell.h"
@interface SliderCell()
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (nonatomic) int step;
@end
@implementation SliderCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (IBAction)valueChanged:(UISlider* )sender {
    if(self.step != 0)
    {
        int newValue =((int)((self.slider.value + self.step/2 ) / self.step) * self.step);
        [self.slider setValue:newValue animated:NO];
    }
    self.valueLabel.text = [NSString stringWithFormat:@"%.0f", sender.value];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)slidingStopped:(UISlider *)sender {
    if(self.delegate)
    {
        [self.delegate slidingDidStop:self.path atValue:sender.value];
    }
}
-(void)initSliderValues:(NSRange)range beginAt:(float)value
{
   self.slider.minimumValue = range.location;
   self.slider.maximumValue = range.location + range.length;
   self.slider.value = value;
   self.valueLabel.text = [NSString stringWithFormat:@"%.0f", self.slider.value];
    
}
-(void)initSliderValues:(NSRange)range beginAt:(float)value step:(int)step
{
    [self initSliderValues:range beginAt:value];
    self.slider.continuous = YES;
    [self.slider addTarget:self
               action:@selector(valueChanged:)
     forControlEvents:UIControlEventValueChanged];
    self.step = step;
    
    
}

@end
