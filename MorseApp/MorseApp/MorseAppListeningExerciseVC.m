//
//  MorseAppListeningExerciseVCViewController.m
//  MorseApp
//
//  Created by Admin on 28.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "MorseAppListeningExerciseVC.h"
#import "MorseAppResultVC.h"
#import "MorseAppExercisesSettingsVC.h"
#import "General/UILabel+ShowPopup.h"
#import "General/Transmitter.h"
#import "General/Generator.h"
#import "General/NSString+CompareWithAttributeStringResult.h"
#import "Settings.h"

@interface MorseAppListeningExerciseVC ()
@property (weak, nonatomic) IBOutlet UIImageView *radioView;
@property (weak, nonatomic) IBOutlet UITextField *inputField;
@property (weak, nonatomic) IBOutlet UIButton *reloadButton;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UITextView *correctResultTextView;

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *correctAnswerInfo;

@property (strong,nonatomic) NSMutableArray * resultsData;
@property (strong,nonatomic) Transmitter * transmitter;
@property (strong, nonatomic) NSArray * radioImages;//of UIImage;
@property (strong, nonatomic) Generator * generator;
@property (strong, nonatomic) NSString * currentSequence;
@property (strong, nonatomic) NSTimer * delayTimer;
@property (strong, nonatomic) Settings * settings;

@end

@implementation MorseAppListeningExerciseVC

-(Settings *)settings
{
    if (!_settings) {
        _settings = [Settings sharedSettings];
    }
    return _settings;
}
-(Generator *)generator{
    if (!_generator) {
        _generator = [[Generator alloc] init];
    }
    return _generator;
}
-(NSString *)currentSequence
{
    if (!_currentSequence) {
        _currentSequence = [self.generator nextGroups];
    }
    return _currentSequence;
}
-(NSArray *)radioImages
{
    if (!_radioImages) {
        _radioImages = @[
                         [UIImage imageNamed:@"radio0"],
                         [UIImage imageNamed:@"radio1"],
                         [UIImage imageNamed:@"radio2"],
                         [UIImage imageNamed:@"radio3"],
                         [UIImage imageNamed:@"radio4"]
                         ];
    }
    return _radioImages;
}
-(Transmitter *)transmitter
{
    if (!_transmitter) {
        _transmitter = [Transmitter getCurrentTransmitter];
        _transmitter.delegate = self;
    }
    return _transmitter;
}
-(NSMutableArray *)resultsData
{
    if (!_resultsData) {
        _resultsData = [[NSMutableArray alloc]init];
    }
    return _resultsData;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.reloadButton.layer.cornerRadius = self.reloadButton.frame.size.height/2;
    self.okButton.layer.cornerRadius = self.okButton.frame.size.width/2;
    [self.statusLabel showPopup:@"For begin touch the radio"];
    self.inputField.layer.borderWidth = 1;
    self.inputField.layer.cornerRadius = 5;
    self.inputField.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.inputField.delegate = self;
}
- (void)viewWillDisappear:(BOOL)animated{
    [self.transmitter stop];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)radioTouch:(id)sender {
    self.transmitter.text = self.currentSequence;
    
    if (self.settings.delayBeforeTransmission) {
        if (!self.delayTimer) {
            if (![self.radioView isAnimating])
            {
                self.radioView.animationDuration = 1;
                [self.radioView setAnimationImages:self.radioImages];
                [self.radioView startAnimating];
                [self.inputField becomeFirstResponder];
                self.correctResultTextView.attributedText = nil;
                self.correctAnswerInfo.hidden = YES;
                self.inputField.text = nil;
                self.delayTimer = [NSTimer scheduledTimerWithTimeInterval:self.settings.delayBeforeTransmission
                                                 target:self
                                               selector:@selector(delayTimerFired:)
                                               userInfo:nil
                                                repeats:NO];
            }else{
                [self.transmitter stop];
            }
        }else{
            [self.delayTimer invalidate];
            self.delayTimer = nil;
            if ([self.radioView isAnimating]) {
                [self.radioView stopAnimating];
                [self.radioView setImage:self.radioImages[0]];
            }
            [self.inputField resignFirstResponder];
        }
    }else{
        
        [self.transmitter transmitt];
        if (![self.radioView isAnimating]) {
            self.radioView.animationDuration = 1;
            [self.radioView setAnimationImages:self.radioImages];
            [self.radioView startAnimating];
            [self.inputField becomeFirstResponder];
            self.correctResultTextView.attributedText = nil;
            self.correctAnswerInfo.hidden = YES;
            self.inputField.text = nil;
        }else{
            [self.transmitter stop];
        }
    }

    
}

-(void)delayTimerFired:(NSTimer *)timer
{
    [self.transmitter transmitt];
    [self.delayTimer invalidate];
    self.delayTimer = nil;
}

- (IBAction)endExercise:(id)sender {
    if ([self.resultsData count]) {
        [self performSegueWithIdentifier:@"listeningExerciseToResultSegue" sender:self];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)transmittanceFinished
{
    if ([self.radioView isAnimating]) {
        [self.radioView stopAnimating];
        [self.radioView setImage:self.radioImages[0]];
    }
}
- (void)reload{
    self.currentSequence = [self.generator nextGroups];
    self.correctResultTextView.text = @"";
    self.inputField.text = nil;
    self.correctAnswerInfo.hidden = YES;
    [self.transmitter stop];
}
- (IBAction)reload:(id)sender {
    [self reload];
    [self.statusLabel showPopup:@"New sequence generated"];
    NSLog(@"Generated string : %@", self.currentSequence);
}

- (IBAction)exerciseSettings:(id)sender {
    [self performSegueWithIdentifier:@"receiveExerciseToExercisesSettingsSegue"
                              sender:self];
}

-(void)check
{
    NSMutableArray * correctGroups = [[self.currentSequence componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] mutableCopy];
    [correctGroups removeObjectIdenticalTo:@""];

    NSMutableArray * inputGroups = [[[self.inputField.text uppercaseString] componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]mutableCopy];
    [inputGroups removeObjectIdenticalTo:@""];
    
   
    if ([self arrayOfString:correctGroups areEqualToArray:inputGroups]) {
        [self.statusLabel showPopup:@"Answer is correct! New sequence generated"];
        [self reload];
        self.inputField.text = @"";
        self.correctAnswerInfo.hidden = YES;
        self.correctResultTextView.attributedText = nil;
    }else{
        self.correctResultTextView.attributedText = [self compareCorrectArray:correctGroups withInputed:inputGroups];
        self.correctAnswerInfo.hidden = NO;
        [self.statusLabel showPopup:@"Wrong answer"];

    }
    for (int i = 0; i < [correctGroups count]; i++) {
        if (i >= [inputGroups count]) {
            [self.resultsData addObject:@{correctGroups[i] : @""}];
        }else{
            [self.resultsData addObject:@{correctGroups[i] : inputGroups[i]}];
        }
    }
}
-(BOOL)arrayOfString:(NSArray*)inputArray areEqualToArray:(NSArray *)correctArray
{
    if([inputArray count] != [correctArray count])
    {
        return NO;
    }else
    {
        for (int i = 0; i < [inputArray count]; i++) {
            NSString *correct = [correctArray[i] stringByReplacingOccurrencesOfString:@"Ё" withString:@"Е"];
            NSString *inputed = [inputArray[i] stringByReplacingOccurrencesOfString:@"Ё" withString:@"Е"];
            if (![correct isEqualToString:inputed]) {
                return NO;
            }
        }
    }
    return YES;
}
-(NSAttributedString *)compareCorrectArray:(NSArray*)correctArray withInputed:(NSArray*)inputedArray
{

    
    NSMutableAttributedString *result = [[NSMutableAttributedString alloc]init];
    for (int i = 0; i < [correctArray count]; i++) {
        if (i >= [inputedArray count]) {
            [result appendAttributedString:[@"" highlightDifferentFromString:correctArray[i]
                                                                correctColor:[UIColor whiteColor]
                                                                  wrongColor:[UIColor darkGrayColor]
                                                                     andSize:22]];
        }else{
            [result appendAttributedString:[inputedArray[i] highlightDifferentFromString:correctArray[i]
                                                                            correctColor:[UIColor whiteColor]
                                                                              wrongColor:[UIColor darkGrayColor]
                                                                                 andSize:22]];
        }
        [result appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
    }
    return result;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.inputField) {
        [self check];
        [self.inputField resignFirstResponder];
        [self.transmitter stop];
        return NO;
    }
    return YES;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"listeningExerciseToResultSegue"])
    {
        MorseAppResultVC *resultVC = [segue destinationViewController];
        
        resultVC.resultsData = [self.resultsData copy];
    }else if ([[segue identifier] isEqualToString:@"receiveExerciseToExercisesSettingsSegue"]){
        MorseAppExercisesSettingsVC *settingsVC = [segue destinationViewController];
        settingsVC.receiveExercise = YES;
    }
}
@end
