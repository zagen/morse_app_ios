//
//  SliderCell.h
//  MorseApp
//
//  Created by Admin on 21.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SliderCellDelegate.h"
@interface SliderCell : UITableViewCell
@property (strong,nonatomic) NSIndexPath * path;
@property (strong, nonatomic)id<SliderCellDelegate> delegate;
-(void)initSliderValues:(NSRange)range beginAt:(float)value;
-(void)initSliderValues:(NSRange)range beginAt:(float)value step:(int)step;
@end
