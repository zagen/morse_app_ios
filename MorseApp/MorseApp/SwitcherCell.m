//
//  SwitcherCell.m
//  MorseApp
//
//  Created by Admin on 21.08.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "SwitcherCell.h"

@interface SwitcherCell()
@property (weak, nonatomic) IBOutlet UISwitch *switcher;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
@implementation SwitcherCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)switched:(UISwitch *)sender {
    if(self.delegate)
    {
        [self.delegate checkStateChanged:sender.isOn];
    }
}

-(void) initWithLabelText:(NSString *)text andSwitchValue:(BOOL)value
{
    self.label.text = text;
    self.switcher.on = value;
}
@end
