//
//  MorseAppListeningTextFilesVC.m
//  MorseApp
//
//  Created by Admin on 03.09.14.
//  Copyright (c) 2014 zagen. All rights reserved.
//

#import "MorseAppListeningTextFilesVC.h"
#import "MorseAppSelectTextFileVC.h"
#import "General/TextFileWordReader.h"
#import "Settings.h"
#import "General/Transmitter.h"

@interface MorseAppListeningTextFilesVC ()

@property (weak, nonatomic) IBOutlet UILabel *fileNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *percentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *radioView;
@property (strong, nonatomic) NSArray * images; //of UIImage

@property (weak, nonatomic) IBOutlet UILabel *currentWordLabel;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *gotoPercentButton;

@property (weak, nonatomic) IBOutlet UIButton *okButton;


@property (strong, nonatomic) TextFileWordReader * wordReader;
@property (nonatomic) BOOL isPlay;
@property (strong, nonatomic) Transmitter * transmitter;

@end

@implementation MorseAppListeningTextFilesVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    self.okButton.layer.cornerRadius = self.okButton.frame.size.width/2;
    self.gotoPercentButton.layer.cornerRadius = self.gotoPercentButton.frame.size.width/2;
    
    if ([self.wordReader hasFile]) {
        self.fileNameLabel.text = self.wordReader.filename;
        
    }
    [self updateUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(TextFileWordReader *)wordReader
{
    if (!_wordReader) {
        _wordReader = [[TextFileWordReader alloc]init];
    }
    return _wordReader;
}
-(Transmitter *)transmitter
{
    if (!_transmitter) {
        _transmitter = [Transmitter getCurrentTransmitter];
        _transmitter.delegate = self;
    }
    return _transmitter;
}
-(NSArray *)images
{
    if (!_images) {
        _images = @[
                    [UIImage imageNamed:@"radiosmall0"],
                    [UIImage imageNamed:@"radiosmall1"],
                    [UIImage imageNamed:@"radiosmall2"],
                    [UIImage imageNamed:@"radiosmall3"],
                    [UIImage imageNamed:@"radiosmall4"]
                    ];
    }
    return _images;
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"openTextFileSegue"]) {
        MorseAppSelectTextFileVC * nextVC = [segue destinationViewController];
        nextVC.delegate = self;
    }
}
- (IBAction)forward:(id)sender {
    self.isPlay = NO;
    [self.transmitter stop];
    [self.wordReader seekToNextWord];
    [self updateUI];
}
- (IBAction)backward:(id)sender {
    self.isPlay = NO;
    [self.transmitter stop];
    [self.wordReader seekToPreviousWord];
    [self updateUI];
}
- (IBAction)fastForward:(id)sender {
    self.isPlay = NO;
    [self.transmitter stop];
    [self.wordReader seekToNextTenWord];
    [self updateUI];
}
- (IBAction)fastBackward:(id)sender {
    self.isPlay = NO;
    [self.transmitter stop];
    [self.wordReader seekToPreviousTenWord];
    [self updateUI];
}
- (IBAction)playPause:(id)sender {
    if (self.isPlay || ![self.wordReader nextWord]) {
        self.isPlay = NO;
        [self.transmitter stop];
        
    }else
    {
       self.transmitter.text = [self.wordReader nextWord];
        [self.transmitter transmitt];
        self.isPlay = YES;
    }
    [self updateUI];
    NSLog(@"Is play : %i", self.isPlay);
}
- (IBAction)gotoPercent:(id)sender {
    UIAlertView * gotoPercentDialog = [[UIAlertView alloc] initWithTitle:@"Go to percent" message:@"Input percent of text you want to go to" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"Ok",nil];
    gotoPercentDialog.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField * tf = [gotoPercentDialog textFieldAtIndex:0];
    if (tf) {
        tf.keyboardType =UIKeyboardTypeDecimalPad;
    }
    [gotoPercentDialog show];
    gotoPercentDialog = nil;

}
-(BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    UITextField * textField = [alertView textFieldAtIndex:0];
    if (textField) {
        NSScanner * scanner = [NSScanner scannerWithString:textField.text];
        float textFieldValue;
        if([scanner scanFloat:&textFieldValue] && (textFieldValue >=0 && textFieldValue <= 100) ) {
            return YES;
        }
    }
    return NO;
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        UITextField * textField = [alertView textFieldAtIndex:0];
        if (textField) {
            NSScanner * scanner = [NSScanner scannerWithString:textField.text];
            float textFieldValue;
            if([scanner scanFloat:&textFieldValue] && (textFieldValue >=0 && textFieldValue <= 100) ) {
                [self.wordReader gotoPercent:textFieldValue];
                [self updateUI];
            }
        }
    }
}

-(void)selectedTextFileWithName:(NSString *)filename
{
    self.wordReader.filename = filename;
    self.fileNameLabel.text = filename;
    [self updateUI];

    
}
-(void)transmittanceFinished
{
    if (self.isPlay) {
        [self.wordReader seekToNextWord];
        
        if ([self.wordReader nextWord]) {
            [self.transmitter transmitt:[self.wordReader nextWord]];
        }else{
            self.isPlay = NO;
        }
        [self updateUI];
    }
}
-(void) updateUI
{
    self.currentWordLabel.text = ([self.wordReader nextWord])? [self.wordReader nextWord] : [self.wordReader previousWord];
    NSLog(@"%f", self.wordReader.percent);
    self.percentLabel.text = [NSString stringWithFormat:@"%.2f%%", self.wordReader.percent];
    if (!self.isPlay) {
        [self.playButton setImage:[UIImage imageNamed:@"text_play"] forState:UIControlStateNormal];
        if ([self.radioView isAnimating]) {
            [self.radioView stopAnimating];
            [self.radioView setImage:self.images[0]];
        }
    }else
    {
        [self.playButton setImage:[UIImage imageNamed:@"text_pause"] forState:UIControlStateNormal];
        if (![self.radioView isAnimating]) {
            [self.radioView setAnimationImages:self.images];
            self.radioView.animationDuration = 1;
            [self.radioView startAnimating];
        }
        

    }
}


-(void)viewWillDisappear:(BOOL)animated
{
    [self.wordReader saveProgress];
    self.isPlay = NO;
    [self.transmitter stop];
}
@end
